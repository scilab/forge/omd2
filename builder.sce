mode(-1);
lines(0);

TOOLBOX_VERSION  = [0 0 3];
TOOLBOX_NAME  = "sop";
TOOLBOX_TITLE = "Scilab Optimization Platform"; // TODO this name causes problems for documentation, this bug will be fixed in Scilab 5.4.0-a2
toolbox_dir   = get_absolute_file_path("builder.sce");

mprintf(gettext("\nBuilding %s (Version %d.%d.%d)\n"), TOOLBOX_TITLE, TOOLBOX_VERSION(1), TOOLBOX_VERSION(2), TOOLBOX_VERSION(3));
mprintf("=====================================================\n");

// Check Scilab version
// ======================
versionOK = %T;
mprintf(gettext("Checking if Scilab version >= 5.4.0: "))
try
    v = getversion("scilab");
catch
    versionOK = %F;
end
if v(2) < 4 then
    // Scilab 5.4.0 needed for temporary file names
    versionOK = %F;
end
if versionOK then
    mprintf(gettext("OK\n"));
else
    mprintf(msprintf(gettext("FAILED (Found version %d.%d.%d)\n"), v(1), v(2), v(3)));
    return
end
clear v versionOK

// Check development_tools module avaibility
// =========================================
mprintf(gettext("Checking if ''%s'' module is installed: "), "development_tools")
if ~with_module("development_tools") then
  error(msprintf(gettext("%s module not installed."), "development_tools"));
end
mprintf(gettext("OK\n"));

// Create the up-to-date sopVersion function
// =========================================
mprintf(gettext("Updating ''%s'' file: "), "sopVersion.sci")
txt = mgetl(fullfile(toolbox_dir, "etc", "sopVersion.in"));
txt = strsubst(txt, "<TOOLBOX_VERSION>", sci2exp(TOOLBOX_VERSION));
mputl(txt, fullfile(toolbox_dir, "macros", "core", "sopVersion.sci"));
clear txt
mprintf(gettext("OK\n"));

// Build
// =====
tbx_builder_macros(toolbox_dir);

compilerFound = %F;
mprintf(gettext("Searching for compiler: "));
if getos()=="windows" & haveacompiler() then
    compilerFound = %T;
else
    unix("gcc --version");
    compilerFound = %T;
end

if compilerFound then
    mprintf(gettext("OK\n"));
    ilib_verbose(0);
    tbx_builder_src(toolbox_dir);
    tbx_builder_gateway(toolbox_dir);
else
    mprintf(gettext("KO (Gateway functions will not be available)\n"));
end
clear compilerFound
tbx_builder_help(toolbox_dir);
tbx_build_loader(TOOLBOX_NAME, toolbox_dir);
tbx_build_cleaner(TOOLBOX_NAME, toolbox_dir);

// Clean variables
// ===============
clear toolbox_dir TOOLBOX_NAME TOOLBOX_TITLE TOOLBOX_VERSION;
