mode(-1);

// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

testCasePath = get_absolute_file_path("initTestCase1.sce");

exec(testCasePath + "compute_criteria.sci", -1);
exec(testCasePath + "make_index.sci", -1);
exec(testCasePath + "testcase1PostProcessing.sci", -1);
exec(testCasePath + "get_U_and_p.sci", -1);
exec(testCasePath + "make_mesh_file.sci", -1);
exec(testCasePath + "make_controlDict_file.sci", -1);
exec(testCasePath + "make_profile.sci", -1);
