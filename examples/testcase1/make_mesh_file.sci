function make_mesh_file(param,density,directory)
//function make_mesh_file(param,density,directory)
//--------------------------------------------------------------------------
// Generates the blockMeshDict file for OpenFOAM.
//
// Inputs:
//   - param: shape parameters (1 x 13)
//   - density:
//   - directory: blockMeshDict target directory
//
// Parameters :
//         - npts : nb of discretization points for Bezier curve
//         - density : to tune the meshing density.
//                     Corresponds to the nb of elements on the transverse
//                     direction.
//--------------------------------------------------------------------------
// author: V. Picheny, ECP - date: feb. 2010
//--------------------------------------------------------------------------
//
// Scilab author
// SIREHNA
// G. Jacquenot
// 2010 - 04 - 30

// Nombre d'arguments dans l'appel de la fonction
[%nargout,%nargin] = argn(0)

if %nargin<1 then
  param     = [];
  directory = 'blockMeshDict';
end;


// Parametres
npts    = 20;
fid     = mopen(directory,"wt");


//--------------------------------------------------------------------------
// Calcul du profil et des matrices d''indices
C = make_profile(npts,param);
C(:,[2,npts+4]) = [];

[vertices,blocks,inlet,outlet,fixedWalls,frontAndBack] = make_index(C);

nvertices     = size(vertices,1);
nblocks       = size(blocks,1);
nfixedWalls   = size(fixedWalls,1);
nfrontAndBack = size(frontAndBack,1);

//--------------------------------------------------------------------------
// En-tête

A = "/*--------------------------------*- C++ -*----------------------------------*\\ \n"+...
    "| =========                 |                                                 | \n"+...
    "| \\\\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           | \n"+...
    "|  \\\\    /   O peration     | Version:  1.5                                   | \n"+...
    "|   \\\\  /    A nd           | Web:      http://www.OpenFOAM.org               | \n"+...
    "|    \\\\/     M anipulation  |                                                 | \n"+...
    "\\*---------------------------------------------------------------------------*/ \n"+...
    "FoamFile \n"+...
    "{ \n"+...
    "    version     2.0; \n"+...
    "    format      ascii; \n"+...
    "    class       dictionary; \n"+...
    "    object      blockMeshDict; \n"+...
    "}\n"+...
    "// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * // \n"+...
    " \n"+...
    "convertToMeters 0.001; \n"+...
    " \n"+...
    "vertices \n"+...
    "( \n";

fprintf(fid,A);

//--------------------------------------------------------------------------
// Vertices

for i = 1:nvertices
  fprintf(fid,"    (%0.5g %0.5g %0.5g) \n",vertices(i,1:3));
end;
fprintf(fid,"); \n");

//--------------------------------------------------------------------------
// Blocks
fprintf(fid,"blocks \n( \n");

fprintf(fid,"    hex (%d %d %d %d %d %d %d %d) (%d %d 1) edgeGrading (1 1 1 1 5 5 5 5 1 1 1 1)\n",...
        blocks(1,1:8),density,2*density);

for i = 2:npts+1
  fprintf(fid,"    hex (%d %d %d %d %d %d %d %d) (%d %d 1) simpleGrading (1 1 1)\n",...
              blocks(i,1:8),density,ceil(density/(npts+1)));
end;

fprintf(fid,"    hex (%d %d %d %d %d %d %d %d) (%d %d 1) simpleGrading (1 1 1)\n",...
             blocks(npts+2,1:8),density,ceil(density/2));

for i = npts+3:nblocks-1
  fprintf(fid,"    hex (%d %d %d %d %d %d %d %d) (%d %d 1) simpleGrading (1 1 1)\n",...
              blocks(i,1:8),density,ceil(density/(npts+1)));
end;

fprintf(fid,"    hex (%d %d %d %d %d %d %d %d) (%d %d 1) edgeGrading (1 1 1 1 .2 .2 .2 .2 1 1 1 1)\n",...
            blocks($,1:8),density,2*density);
fprintf(fid,");\n");

//--------------------------------------------------------------------------
// Edges and patches
fprintf(fid,"edges \n( \n);\n \npatches \n( \n");
fprintf(fid,"    patch inlet\n");
fprintf(fid,"    (\n");
fprintf(fid,"        (%d %d %d %d)\n",inlet(1:4));
fprintf(fid,"    )\n");
fprintf(fid,"    patch outlet\n");
fprintf(fid,"    (\n");
fprintf(fid,"        (%d %d %d %d)\n",outlet(1:4));
fprintf(fid,"    )\n");
fprintf(fid,"    wall fixedWalls\n");
fprintf(fid,"    (\n");
for i = 1:nfixedWalls
  fprintf(fid,"        (%d %d %d %d)\n",fixedWalls(i,1:4));
end;
fprintf(fid,"    )\n");
fprintf(fid,"    empty frontAndBack\n");
fprintf(fid,"    (\n");
for i = 1:nfrontAndBack
  fprintf(fid,"        (%d %d %d %d)\n",frontAndBack(i,1:4));
end;
fprintf(fid,"    )\n");
fprintf(fid,"); \nmergePatchPairs \n(\n);\n");
fprintf(fid,"//***********************************************************//");
mclose(fid);
endfunction
