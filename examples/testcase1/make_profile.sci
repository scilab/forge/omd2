function ContourDiscretise = make_profile(npts, param)
//function ContourDiscretise = make_profile(npts, param)
//--------------------------------------------------------------------------
// Genere un profil de conduit d'aeration parametre.
// Fonction appelee par le script make_mesh_file.
//
// Parametre d'entree : nombre de points de discretisation pour une courbe
// de Bezier.
//
// Parametre de sortie : matrice contenant les coordonnees des profils
// superieurs et inferieurs.
//
// Notes :
// a) Dimensions choisies en accord avec le document "Parametrage 2.ppt"
// b) Les distances sont en mm, les angles en degres.
//
//--------------------------------------------------------------------------

// Nombre d'arguments dans l'appel de la fonction
[%nargout,%nargin] = argn(0)

//% Donnees fixes

// embouchure "haute" du conduit
P1 = [ 79.315, 373.35 ];
P2 = [ 18.699, 373.35 ];

// embouchure "haute" du S
P3 = [ 79.315, 223.35 ];
P4 = [ 18.699, 223.35 ];

// embouchure "basse" du S
P9  = [ 119.765, 139.085 ];
P10 = [  79.193, 147.339 ];

// embouchure "basse"
P11 = [ 91.859,  1.894 ];
P12 = [ 51.282, 10.149 ];

//% Parametres a optimiser

if %nargin == 0
    npts = 200;
end
if %nargin < 2 | isempty(param)

    // partie rectiligne a l'interieur du S
    // (parametrage conforme au document ppt)
    x1 =  7.637;
    x2 = 26.635;
    x3 = 12.683;
    x4 =  7.604;
    x5 = 54.500;

    // parametres des courbes de Bezier
    // (deux parametres pour chaque courbe)

    a1 =  2;  b1 =  2;  // P3/P5
    a2 = 30;  b2 = 30;  // P4/P6
    a3 = 30;  b3 = 30;  // P7/P9
    a4 =  5;  b4 =  5;  // P8/P10
  else
    x1 = param(1);
    x2 = param(2);
    x3 = param(3);
    x4 = param(4);
    x5 = param(5);
    a1 = param(6);     b1 = param(7);   // P3/P5
    a2 = param(8);     b2 = param(9);    // P4/P6
    a3 = param(10);    b3 = param(11);   // P7/P9
    a4 = param(12);    b4 = param(13);  // P8/P10
end

// Discretisation du contour
// ContourDiscretise(1,:): abcisses de la partie superieure
// ContourDiscretise(2,:): ordonnees de la partie superieure
// ContourDiscretise(3,:): abcisses de la partie inferieure
// ContourDiscretise(4,:): ordonnees de la partie inferieure

ContourDiscretise = discret( ...
    P1,P2,P3,P4,P9,P10,P11,P12, ...
    x1,x2,x3,x4,x5,a1,b1,a2,b2,a3,b3,a4,b4, npts );

// if %nargin == 0
//     clf()
//     plot(ContourDiscretise(1,:),ContourDiscretise(2,:),'r');
//     plot(ContourDiscretise(3,:),ContourDiscretise(4,:),'b');
// end
endfunction

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function V = discret(P1,P2,P3,P4,P9,P10,P11,P12,d,t,...
                     d2,d5,d3,a1,b1,a2,b2,a3,b3,a4,b4,n)
//fonction rendant la forme discretisee du conduit
//P1,...,P4,P9,...P12 sont les points des extremites du conduit
//d,t,d2,d5,d3 sont les paramatres imposes (dans l'ordre: x1,...,x5)
//a1,b1,a2,b2,a3,b3,a4,b4 sont les parametres des courbes de Bezier
//n: nombre de point pour chaque courbe

t = t*%pi/180;

// Points Variables:

th   = -atan((P3(2)-P1(2)),(P3(1)-P1(1)));
R    = [[cos(th);-sin(th)],[sin(th);cos(th)]];
Paux = P3+(R*[0;d])';
P5   = Paux+d5*(R*[cos(t);sin(t)])';
P7   = P5+(R*[sin(t);cos(t)])'*d2;
P6   = P5+d3*(R*[cos(t);-sin(t)])';
P8   = P7+d3*(R*[cos(t);-sin(t)])';

// Courbes:

Pb1 = (P5-P7)/(sqrt((P7-P5)*(P7-P5)'))*b1 + P5;
Pa1 = (P3-P1)/(sqrt((P3-P1)*(P3-P1)'))*a1 + P3;
[B1]=Courbe(P3,Pa1,Pb1,P5,n);

Pb2 = (P6-P8)/(sqrt((P8-P6)*(P8-P6)'))*b2 + P6;
Pa2 = (P4-P2)/(sqrt((P4-P2)*(P4-P2)'))*a2 + P4;
[B2]=Courbe(P4,Pa2,Pb2,P6,n);

Pb3 = (P9-P11)/(sqrt((P11-P9)*(P11-P9)'))*b3 + P9;
Pa3 = -(P5-P7)/(sqrt((P7-P5)*(P7-P5)'))*a3 + P7;
[B3]=Courbe(P7,Pa3,Pb3,P9,n);

Pb4 = (P10-P12)/(sqrt((P12-P10)*(P12-P10)'))*b4 + P10;
Pa4 = -(P6-P8)/(sqrt((P8-P6)*(P8-P6)'))*a4 + P8;
[B4]=Courbe(P8,Pa4,Pb4,P10,n);

// Ensemble des points definissant le conduit:

//abcisses de la partie haute du conduit
V(1,:) = [P1(1),P3(1),B1(1,:),P5(1),P7(1),B3(1,:),P9(1),P11(1)];
//ordonnees de la partie haute du conduit
V(2,:) = [P1(2),P3(2),B1(2,:),P5(2),P7(2),B3(2,:),P9(2),P11(2)];
//abcisses de la partie basse du conduit
V(3,:) = [P2(1),P4(1),B2(1,:),P6(1),P8(1),B4(1,:),P10(1),P12(1)];
//ordonnees de la partie basse du conduit
V(4,:) = [P2(2),P4(2),B2(2,:),P6(2),P8(2),B4(2,:),P10(2),P12(2)];
endfunction

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function B = Courbe (P1,P2,P3,P4,n)
//Rend une courbe de Bezier d'ordre 3 reliant P1 a P4 grace aux points de
//controle P2 et P4
//n: nombre de points de la courbe

t = 0:1/n:1-1/n;
//abcisses
B(1,:) = P1(1)*(1-t).^3 + 3*P2(1)*t.*(1-t).^2+3*P3(1)*(t.^2).*(1-t)+P4(1)*t.^3;
//ordonnees
B(2,:) = P1(2)*(1-t).^3 + 3*P2(2)*t.*(1-t).^2+3*P3(2)*(t.^2).*(1-t)+P4(2)*t.^3;

endfunction
