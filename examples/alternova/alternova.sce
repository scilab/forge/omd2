// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

projectFile = sopModulePath() + "examples/alternova/alternova-generated.sop"

if ~isfile(projectFile) then

    alterProj = sopProject();

    // Load data
    sopLoadDataFile(alterProj, sopModulePath() + "examples/alternova/BDconstruction_P1.txt");
    sopLoadDataFile(alterProj, sopModulePath() + "examples/alternova/BDconstruction_P2.txt");
    sopLoadDataFile(alterProj, sopModulePath() + "examples/alternova/BDvalidation_P1.txt", %T);
    sopLoadDataFile(alterProj, sopModulePath() + "examples/alternova/BDvalidation_P2.txt", %T);

    // Sort data
    sopAddResponse(alterProj, ["Dx_201417", "Dx_204471", "Dx_209158", "Nbre_EPMX_ECE14_PID250000"]);
    sopAddFactor(alterProj, 1:sopGetNbVariables(alterProj));

    // Modeling
    // P1M1
    alternovaModelPtr = sopAddModel(sopGetResponse(alterProj, "Dx_201417"), "sopw_alternova");
    sopSetParameter(alternovaModelPtr, "modelfilename", sopModulePath() + "examples/alternova/P1M1.sci")
    alternovaModelPtr = sopAddModel(sopGetResponse(alterProj, "Dx_204471"), "sopw_alternova");
    sopSetParameter(alternovaModelPtr, "modelfilename", sopModulePath() + "examples/alternova/P1M1.sci")

    // P2M0
    alternovaModelPtr = sopAddModel(sopGetResponse(alterProj, "Dx_209158"), "sopw_alternova");
    sopSetParameter(alternovaModelPtr, "modelfilename", sopModulePath() + "examples/alternova/P2M0.sci")
    alternovaModelPtr = sopAddModel(sopGetResponse(alterProj, "Nbre_EPMX_ECE14_PID250000"), "sopw_alternova");
    sopSetParameter(alternovaModelPtr, "modelfilename", sopModulePath() + "examples/alternova/P2M0.sci")
    // P2M1
    alternovaModelPtr = sopAddModel(sopGetResponse(alterProj, "Dx_209158"), "sopw_alternova");
    sopSetParameter(alternovaModelPtr, "modelfilename", sopModulePath() + "examples/alternova/P2M1.sci")
    alternovaModelPtr = sopAddModel(sopGetResponse(alterProj, "Nbre_EPMX_ECE14_PID250000"), "sopw_alternova");
    sopSetParameter(alternovaModelPtr, "modelfilename", sopModulePath() + "examples/alternova/P2M1.sci")
    // P2M2
    alternovaModelPtr = sopAddModel(sopGetResponse(alterProj, "Dx_209158"), "sopw_alternova");
    sopSetParameter(alternovaModelPtr, "modelfilename", sopModulePath() + "examples/alternova/P2M2.sci")
    alternovaModelPtr = sopAddModel(sopGetResponse(alterProj, "Nbre_EPMX_ECE14_PID250000"), "sopw_alternova");
    sopSetParameter(alternovaModelPtr, "modelfilename", sopModulePath() + "examples/alternova/P2M2.sci")

    sopModeling(alterProj);

    sopSaveProject(alterProj, sopModulePath() + "examples/alternova/alternova-generated.sop");

end

sopGui(projectFile);
sopModelingGui();

