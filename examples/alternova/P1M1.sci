function Y = P1M1(X)
// Fonction matlab generee le Tue Dec 13 01:17:24 2011
//	- X : matrice(11, n) de n essais pour les 11 facteurs
//	- Y : matrice(2, n) de n essais pour les 2 reponses

// Ordre des facteurs :
//-----------------------
//  X(1,:) :	PID1999827
//  X(2,:) :	PID250000
//  X(3,:) :	PID250010et250012
//  X(4,:) :	PID999920et1999825
//  X(5,:) :	PID999919
//  X(6,:) :	PID111111
//  X(7,:) :	PID200030et200032
//  X(8,:) :	PID7777777
//  X(9,:) :	PID8888888
//  X(10,:) :	POSITIONGACHE
//  X(11,:) :	TUBEFDT
//
// Ordre des reponses :
//-----------------------
//  Y(1,:) : modeleRMC_Dx_201417
//  Y(2,:) : modeleRMC_Dx_204471

// Check sur la taille de X :
[d,n] = size(X);
if d ~= 11 then
    error("La matrice d''entree X n''a pas le bon nombre de facteurs (nombre de lignes) : " + string(d) + " au lieu de 11 !");
end

// Normalisation des facteurs
lesMinX = [ 4.000000e-001, 4.000000e-001, 1.000000e+000, 1.500000e+000, 2.000000e+000, 4.000000e-001, 1.000000e+000, 8.000000e-001, 8.000000e-001, 1.000000e+000, -1.0];
lesMaxX = [ 5.000000e+000, 5.000000e+000, 4.000000e+000, 4.000000e+000, 5.000000e+000, 5.000000e+000, 5.000000e+000, 1.500000e+000, 1.500000e+000, 4.000000e+000, 1.0];

lesMinX = (ones(size(X,2),1) * lesMinX)';
lesMaxX = (ones(size(X,2),1) * lesMaxX)';

X = (X - lesMinX)./(lesMaxX - lesMinX);
X = 2*X-1;

// Formules des modeles

Reponse_1 = -4.126250e-002.*X(1,:) ...
	+5.245553e-001.*X(2,:) ...
	+4.928809e-002.*X(3,:) ...
	+9.377405e-003.*X(4,:) ...
	-4.373419e-002.*X(5,:) ...
	+1.918147e-001.*X(6,:) ...
	+6.350121e-002.*X(7,:) ...
	-1.574477e-001.*X(8,:) ...
	+4.871209e-002.*X(10,:) ...
	+3.336287e-002.*(X(1,:).*X(1,:)) ...
	-5.185139e-001.*(X(2,:).*X(2,:)) ...
	+1.137054e-001.*(X(3,:).*X(3,:)) ...
	+4.483303e-003.*(X(4,:).*X(4,:)) ...
	-1.004836e-001.*(X(5,:).*X(5,:)) ...
	-1.677855e-001.*(X(6,:).*X(6,:)) ...
	-1.383203e-002.*(X(7,:).*X(7,:)) ...
	-9.925036e-002.*(X(10,:).*X(10,:)) ...
	+7.562871e-001;

Reponse_2 = 2.163268e-001.*X(5,:) ...
	-2.222244e-002.*X(6,:) ...
	-4.800368e-001.*X(8,:) ...
	-4.051448e-001.*(X(5,:).*X(5,:)) ...
	+1.462758e-002.*(X(6,:).*X(6,:)) ...
	+2.365361e-001;

// Anti-normalisation eventuelle
Reponse_1 = Reponse_1 .* ones(1,size(X,2));
Reponse_2 = Reponse_2 .* ones(1,size(X,2));
Reponse_1 = (Reponse_1 + 1) * 7.206700e+001 /2 + 2.120000e+003;
Reponse_2 = (Reponse_2 + 1) * 1.085830e+002 /2 + 2.031417e+003;

Y = [Reponse_1 ; Reponse_2];

endfunction
// Fin de la fonction generee
