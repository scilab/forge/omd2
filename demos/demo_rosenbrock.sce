function [x0,xmin,xmax,constr_rhs, constr_lhs] = fobj_rosen(x,ind)
  x0         = [-1; 1];
  xmin       = [-2; -3];
  xmax       = [2; 3];
  constr_rhs = [];
  constr_lhs = [];

  f     = (1-x(1))^2 + 100*(x(2) - x(1)^2)^2;
  df(1) = -400*x(1)*(x(2) - x(1)^2) - 2*(1-x(1));
  df(2) = 200*(x(2) - x(1)^2);

  select ind
    case 1 then
      // [x0, xmin, xmax, constr_rhs, constr_lhs] = fobj(x, ind);
      // Fields are already filled
    case 2 then
      // [f,df] = fobj(x, ind);
      x0   = f;
      xmin = df;
    case 3 then
      // [f,g] = fobj(x, ind);
      x0   = f;
      xmin = [];
    case 4 then
      // [df,dg] = fobj(x, ind);
      x0   = df;
      xmin = [];
    case 5 then
      // [f,df,g,dg] = fobj(x, ind);
      x0         = f;
      xmin       = df;
      xmax       = [];
      constr_rhs = [];
    else
      error('wrong objective function mode\n');
  end
endfunction
