// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012 - DIGITEO - Vincent COUVERT
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// This file is released under the 3-clause BSD license. See COPYING-BSD.

function builder_gw_c()

    includes_src_c = ilib_include_flag(get_absolute_file_path("builder_gateway_c.sce") + "../../src/c");

    // PutLhsVar managed by user in sci_sum and in sci_sub
    // if you do not this variable, PutLhsVar is added
    // in gateway generated (default mode in scilab 4.x and 5.x)
    WITHOUT_AUTO_PUTLHSVAR = %t;

    tbx_build_gateway("sopgw_c", ..
                      ["paretofront", "sci_paretofront"], ..
                      "sci_paretofront.c", ..
                      get_absolute_file_path("builder_gateway_c.sce"), ..
                      "../../src/c/libsopsrc_c", ..
                      "", ..
                     includes_src_c);
endfunction

builder_gw_c();
clear builder_gw_c; // remove builder_gw_c on stack
