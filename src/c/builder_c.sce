// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012 - DIGITEO - Vincent COUVERT
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function builder_c()
    src_c_path = get_absolute_file_path("builder_c.sce");

    CFLAGS = ilib_include_flag(src_c_path);

    tbx_build_src("sopsrc_c",      ..
                  "paretofront.c",    ..
                  "c", ..             ..
                  src_c_path,         ..
                  "",                 ..
                  "",                 ..
                  CFLAGS);
endfunction

builder_c();
clear builder_c; // remove builder_c on stack
