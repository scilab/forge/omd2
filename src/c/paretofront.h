#ifndef __PARETOFRONT_H__
#define __PARETOFRONT_H__
/*
 * PARETOFRONT returns the logical Pareto Front of a set of points.
 *      synopsis:   front = paretofront(M)
 * INPUT ARGUMENT
 *      - M         n x m array, of which (i,j) element is the j-th objective
 *                  value of the i-th point;
 *  OUTPUT ARGUMENT
 *      - front     n x 1 logical vector to indicate if the corresponding
 *                  points are belong to the front (true) or not (false).
 * Matlab version: By Yi Cao at Cranfield University, 31 October 2007
 */

/*
 * Scilab version based on Matlab version and developed in the context of:
 * - OMD2 project (French National Agency for Research)
 * - CSDL project (Systematic competitiveness cluster)
 */

void paretofront(int* front, double* M, unsigned int row, unsigned int col);

#endif /* __PARETOFRONT_H__ */

