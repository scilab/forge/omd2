// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopGraphSensibility(responseIndex)

    h = findobj("Tag", "sopmodelinggui");
    sopProj = sopGetCurrentProject(h);

    if size(sopProj.responses(responseIndex).models)==0 then
        messagebox("Reponse ''" + sopProj.responses(responseIndex).name + "'' has no model.", "Error", "error");
        return
    end
    for kModel = 1:size(sopProj.responses(responseIndex).models)
        if isempty(sopProj.responses(responseIndex).models(kModel).coeffs) then
            messagebox("Reponse ''" + sopProj.responses(responseIndex).name + "'' has not been modeled (Model #" + string(kModel) + ").", "Error", "error");
            return
        end
    end

    // Data computing
    // Screening size
    screnningSize = 2; // TODO must be a user selected value

    screening = [];
    screeningData = [];
    screeningPrediction = [];

    factorsNumber = size(sopProj.factors);
    responsesNumber = size(sopProj.responses);

    varMorris = list();
    for kFact=1:factorsNumber
        varMorris(kFact) = [];
    end

    // Visualization
    fig = sopFigure(_("Sensibility analysis") + " - " + sopProj.responses(responseIndex).name, [100+20*responseIndex 100+20*responseIndex 700 500]);

    for kModel = 1:size(sopProj.responses(responseIndex).models) 
        // creation du screening et evaluation du modele dessus
        h = waitbar(0, _("Model prediction on screening data ..."));

        nb_sim = (screnningSize^factorsNumber);
        for ind_dec=1:nb_sim
            waitbar((ind_dec-1)/(nb_sim-1), h);

            // creation du point de screening (entre 1 et n)
            str_dec = dec2base((ind_dec-1), screnningSize, factorsNumber);
            for ind_str_dec=1:factorsNumber
                screening(ind_dec,ind_str_dec) = 1 + eval(part(str_dec,ind_str_dec));
            end

            // normalisation Xelp3
            screeningData(ind_dec,:) = 2*(screening(ind_dec,:)-1)/(screnningSize-1) - 1;

            // prediction
            [screeningPrediction(ind_dec),tmp] = sopPrediction(sopProj, responseIndex, kModel, screeningData(ind_dec,:));
        end

        close(h);


        // Calcul des coefficients
        balayage = [];
        h = waitbar(0, _("Model Variance on screening data ..."));
        nb_balayage_total = ((screnningSize^factorsNumber))/screnningSize;

        // pour chaque facteur, on calcul son indice de sensibilite
        for ind_facteur=1:factorsNumber

            saut = screnningSize^(factorsNumber-ind_facteur);

            init = 0;
            // calcul de tous les balayages possibles
            for ind_bal=1:nb_balayage_total
                waitbar((ind_facteur-1 + 1/factorsNumber*(ind_bal-1)/nb_balayage_total)/factorsNumber,h);

                // cas (particulier) ou la fin du deroulement des lignes du screening est atteinte
                // et qu'il faire un saut de init.
                if modulo(ind_bal-1, screnningSize^(factorsNumber-ind_facteur))==0 then
                    if init==0 then
                        init = 1;
                        init_save = init;
                    else
                        init = init_save + screnningSize^(factorsNumber-ind_facteur+1);
                        init_save = init;
                    end
                    // cas (general) ou il n'y a pas de saut d'initialisation de balayage a faire
                    // on deroule les lignes n fois, avec n=taille du balayage="screnningSize"
                else
                    init = init+1;
                end

                // resultat : un balayage particulier
                // la taille du vect "ind_balayage" est donc egale a "screnningSize"
                ind_balayage = init:saut:(init + saut*screnningSize - 1);

                // calcul de la fonction predite, sur ce balayage particulier
                balayage = screeningPrediction(ind_balayage);
                for i=1:(screnningSize-1)
                    for j=(i+1):screnningSize
                        varMorris(ind_facteur)($+1) = (balayage(j)-balayage(i))/(j-i)*(screnningSize-1);
                    end
                end
            end

        end

        close(h);

        classement_facteurs = zeros(1,factorsNumber);

        seuil = 0;

        histoQuantity = list();
        histoValue = list();

        // affichage des sensibilites pour chaque reponse

        drawlater();

        a = sopSubplot(size(sopProj.responses(responseIndex).models), 1, kModel);

        // ordonnancement
        for kFact=1:factorsNumber
            mean_var(kFact) = mean(varMorris(kFact));
            max_var(kFact) = max(varMorris(kFact));
            min_var(kFact) = min(varMorris(kFact));
            max_abs_var(kFact) = max(abs(varMorris(kFact)));

            // Calculate histogram
            mn = min(varMorris(kFact));
            mx = max(varMorris(kFact));
            n = 30;
            if mn==mx then
                mn = mn - floor(n/2) - 0.5;
                mx = mx + ceil(n/2) - 0.5;
            end
            binwidth = (mx-mn) ./ n;
            limits = mn + binwidth * (0:n);
            limits($) = mx;
            f = zeros(1,size(limits,'*')-1);
            for kLimit = 1:(size(limits,'*')-1)
                if kLimit<size(limits,'*')-1 then
                    f(kLimit) = sum(bool2s((varMorris(kFact)>=limits(kLimit))&(varMorris(kFact)<limits(kLimit+1))));
                else // To be sure the max value occurences at taken into account
                    f(kLimit) = sum(bool2s((varMorris(kFact)>=limits(kLimit))&(varMorris(kFact)<=limits(kLimit+1))));
                end
            end
            f($+1) = 0;

            histoQuantity(kFact) = 0.5*f/(sum(f));
            histoValue(kFact) = limits;
        end
        [tmp ordre] = gsort(-max_abs_var, "r", "i"); // classement du + grand au + petit

        // [BEGIN] Statictical error display
        eps = 0;
        xfill= [1/2 factorsNumber+1/2 factorsNumber+1/2 1/2];

        // Cross validation R2 (also called Q2) TODO create a function to be reused is sopGraphCorrelation
        crossValidResid = [];
        crossValidPred = [];

        learningPoints = sopGetDoe(sopProj, responseIndex, "learningpoints");
        learningPoints    = sopDoeNormalization(learningPoints);

        // Measured response
        // TODO manage transfo
        mesuredResponse = sopProj.responses(responseIndex).values(sopProj.responses(responseIndex).valuestype == 1);
        for p=1:size(learningPoints,1)
            // Remove one point from DOE and re-compute model
            sopProj = sopModeling(sopProj, ..
                                      learningPoints([1:(p-1) (p+1):size(learningPoints,1)],:), ..
                                      mesuredResponse([1:(p-1) (p+1):size(learningPoints,1)],:), ..
                                      responseIndex, kModel)

            // Prediction
            [crossValidPred(p), predictionError] = sopPrediction(sopProj, responseIndex, kModel, learningPoints(p,:));
            // TODO manage transfo

            // Residuals
            crossValidResid(p) = mesuredResponse(p) - crossValidPred(p);
        end

        // TODO Add validation points handling

        stdCrossValidResid  = sqrt(mean(crossValidResid.^2));
        yfill = [-1 -1 1 1].*stdCrossValidResid;

        a.auto_clear = "off";
        xfpoly(xfill, yfill);
        e = gce();
        e.background = addcolor([1 0.8 0.8]);
        e.foreground = addcolor([1 0.8 0.8]);
        // [END] Statictical error display

        // [BEGIN] Factors ranking display
        for kFact=1:factorsNumber
            XX = [kFact+histoQuantity(ordre(kFact)) kFact-histoQuantity(ordre(kFact))($:-1:1)];
            YY = [histoValue(ordre(kFact)) histoValue(ordre(kFact))($:-1:1)]
            xfpoly(XX, YY);
            e = gce();
            e.background = addcolor([1 0.5 0.5]);
            e.foreground = addcolor([1 0.5 0.5]);
            plot(kFact, mean_var(ordre(kFact)), "k.", "MarkerSize", 4);
        end
        // [END] Factors ranking display

        // Horizontal line
        plot([1/2 factorsNumber+1/2],[0 0], "k");

        a.data_bounds(:,1) = [1/2 ; factorsNumber+1/2];
        a.tight_limits = "on";

        ylabel(sopProj.responses(responseIndex).name);

        for kFact=1:factorsNumber
            xlab(kFact) = sopProj.factors(kFact).name;
            if ~isempty(strindex(xlab(kFact), "_")) then
                xlab(kFact) = part(xlab(kFact), 1:(min(strindex(xlab(kFact), "_"))-1))
            end
        end

        a.x_ticks.labels = [""; xlab(ordre); ""];
        a.data_bounds(:,2) = [-1.1;1.1]*max(max_abs_var);
        a.box = "on";
        
        a.title.text = sopProj.responses(responseIndex).models(kModel).description;
        drawnow();
    end

endfunction

function s = dec2base(d,b,nin)
    rhs = argn(2);
    n = max(1, round(log2(d+1)/log2(b)));
    while or(b^n <= d)
        n = n + 1;
    end
    if rhs == 3 then
        n = max(n,nin);
    end
    s(n) = d-fix(d/b)*b;
    while or(d) & n >1
        n = n - 1;
        d = floor(d/b);
        s(n) = d-fix(d/b)*b;
    end
    symbols = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    s = part(symbols, s+1);
endfunction

