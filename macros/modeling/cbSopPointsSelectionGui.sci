// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function cbSopPointsSelectionGui(gcboTag)

    if argn(2)<1 then
        gcboTag = get(gcbo, "Tag");
    end

    sopPointsGui = findobj("Tag", "soppointsselectiongui");
    sopproject = sopGetCurrentProject(sopPointsGui);

    responsesMenu = findobj("Tag", "responses_menu");
    responseIndex = 1; // Default
    respMenuChildren = responsesMenu.children;
    for kResp=1:size(respMenuChildren, "*")
        if respMenuChildren(kResp).checked=="on" then
            responseIndex = size(respMenuChildren, "*") - kResp + 1;
        end
    end
    
    pointsListbox = findobj("Tag", "points_listbox");
    selectedPoint = pointsListbox.value;

    select gcboTag
    case "points_listbox"
        // Nothing to do
    case "learningpoint_checkbox"
        // Insertion made by steps because of a Scilab bug (not simply reproduced)
        values = sopproject.responses(responseIndex).valuestype;
        values(selectedPoint) = 1;
        sopproject.responses(responseIndex).valuestype = values;
    case "validationpoint_checkbox"
        // Insertion made by steps because of a Scilab bug (not simply reproduced)
        values = sopproject.responses(responseIndex).valuestype;
        values(selectedPoint) = 2;
        sopproject.responses(responseIndex).valuestype = values;
        if selectedPoint==sopproject.doecenterpointindex then // Center point pust be a learning point
            sopproject.doecenterpointindex = 0;
        end
    case "badpoint_checkbox"
        // Insertion made by steps because of a Scilab bug (not simply reproduced)
        values = sopproject.responses(responseIndex).valuestype;
        values(selectedPoint) = 0;
        sopproject.responses(responseIndex).valuestype = values;
        if selectedPoint==sopproject.doecenterpointindex then // Center point pust be a learning point
            sopproject.doecenterpointindex = 0;
        end
    case "doecenter_checkbox"
        if gcbo.value==gcbo.max then
            sopproject.doecenterpointindex = selectedPoint;
        else
            sopproject.doecenterpointindex = 0;
        end
    case "doenumerotation_popup"
        selectedResp = get(gcbo, "Value");
        doeNumVar = sopproject.doenumerotation;
        if selectedResp==1 then // Point index
            if isempty(doeNumVar) then
                return // Nothing to do because has never been set
            end
            newVariables = list();
            indexinfile = doeNumVar.indexinfile;

            kVar = 1;
            while kVar<=size(sopproject.variables) & sopproject.variables(kVar).indexinfile < indexinfile
                newVariables($+1) = sopproject.variables(kVar);
                kVar = kVar + 1;
            end
                
            newVariables($+1) = doeNumVar;
            sopproject.doenumerotation = [];

            while kVar<=size(sopproject.variables)
                newVariables($+1) = sopproject.variables(kVar);
                kVar = kVar + 1;
            end
            sopproject.variables = newVariables;
        else
            if typeof(sopproject.variables(selectedResp - 1).values)<>"string" then
                error("Invalid selection !!"); // TODO
            end
            if ~isempty(doeNumVar) then
                error("Already set"); // TODO better management of this case
            end
            sopproject.doenumerotation = sopproject.variables(selectedResp - 1);
            sopproject.variables(selectedResp - 1) = null();
        end
    case "currentresponse_menu"
        if get(gcbo, "Checked")=="on" then
            // Nothing to do
        else
            reponsesMenu = findobj("Tag", "responses_menu");
            responsesMenuChildren = reponsesMenu.children;
            for kResp=1:size(responsesMenuChildren, "*")
                responsesMenuChildren(kResp).checked = "off";
            end
            set(gcbo, "Checked", "on");
        end
    case "cancel_btn"
        btn = messagebox(_("Do you want to quit Point Selection GUI?"), _("Quit"), "question", ["Yes" "No"], "modal");
        if btn==1 then        
            passingValueUicontrol = findobj("Tag", "waitingforpointsselgui");
            set(passingValueUicontrol, "Userdata", []);
            delete(sopPointsGui);
            sopSetGuiEnable(findobj("tag", "sopmodelinggui"), "on");
            updateSopModelingGui();
        end
        return // To avoid call of update GUI function
    case "ok_btn"
        passingValueUicontrol = findobj("Tag", "waitingforpointsselgui");
        set(passingValueUicontrol, "Userdata", sopproject);
        delete(sopPointsGui);
        sopSetGuiEnable(findobj("tag", "sopmodelinggui"), "on");
        updateSopModelingGui();
        return // To avoid call of update GUI function
    else
        error("Unknown object with tag: " + gcboTag);
    end
disp(sopproject)
    sopSetCurrentProject(sopPointsGui, sopproject);

    upSopPointsSelectionGui();

endfunction
