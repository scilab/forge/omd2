// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopProj = sopModeling(sopProj, selectedPoints, selectedResponseValues, responseIndex, modelIndex)

    // TODO check inputs
    
    // Used with 2 inputs for response modeling
    // Used with 4 inputs for correlation graph

    sopLog(_("Modeling..."));
    sopProj.modelexecstatus = sopStatus("Running");

    rhs = argn(2);
    
    if rhs<4 then
        responseIndex = 1:size(sopProj.responses);
    end    
    
    for kResp = responseIndex
        
        if rhs<2 then // All responses
            selectedPoints = sopGetDoe(sopProj, kResp, "learningpoints"); // Only use good points (not validation, not bad)
        end
        
        //normalizedSelectedPoints = sopDoeNormalization(selectedPoints);
        //validationPoints = sopDoeNormalization(sopGetDoe(sopProj, kResp, "validationpoints"));
        validationPoints = sopGetDoe(sopProj, kResp, "validationpoints");
        
        responsesValuesType = sopProj.responses(kResp).valuestype;
        responseValues = sopProj.responses(kResp).values;
        if rhs<3 then
            selectedResponseValues = responseValues(responsesValuesType == 1); // Only use good points (not validation, not bad)
        end
        validationResponses = responseValues(responsesValuesType == 2);

        if rhs<5 then // All models
            modelIndex = 1:size(sopProj.responses(kResp).models);
        end    
        
        // Call all modelers
        allResponses = sopProj.responses#;
        for kModel = modelIndex
            allModels = allResponses(kResp).models#;
            // Call modeler
            modelerFunction = sopProj.responses(kResp).models(kModel).funname;
            ierr = sopCallWrapper(modelerFunction,  "model", allModels(kModel));
            if ierr<>0 then
                [a,b,c,d] = lasterror();
                disp(d,c,b,a)
                pause
                sopProj.modelexecstatus = sopStatus("Failed");
                sopError("Modeling: FAILED")
                return
            end
            
        end
    end

    sopProj.modelexecstatus = sopStatus("Done");
    sopLog("Modeling: DONE");

endfunction
