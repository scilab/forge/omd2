// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopGraphReponse2D(responseIndex)

    h = findobj("Tag", "sopmodelinggui");
    sopProj = sopGetCurrentProject(h);

    rhs = argn(2);
    
    if rhs==1 then // First launch
        if isempty(responseIndex) then // No response selected
            responseIndex = 1;
        end
        if size(sopProj.responses(responseIndex).models)==0 then
            messagebox("Reponse ''" + sopProj.responses(responseIndex).name + "'' has no model.", "Error", "error");
            return
        end
        for kModel = 1:size(sopProj.responses(responseIndex).models)
            if isempty(sopProj.responses(responseIndex).models(kModel).coeffs) then
                messagebox("Reponse ''" + sopProj.responses(responseIndex).name + "'' has not been modeled (Model #" + string(kModel) + ").", "Error", "error");
                return
            end
        end

        figHeight = 500;
        f = sopFigure(_("2D Response Visualization - ") + sopProj.responses(responseIndex).name, ..
                        [100+20*responseIndex 100+20*responseIndex 700 figHeight], "2Drespvisu");
                        
        [btnbg, btnHeight, margin, figbg] = sopGuiParameters("ButtonBackground", "ButtonHeight", "Margin", "FigureBackground");

        indaxeX = 1;
        indaxeY = 2;

        labelWidth = 60;
        popupWidth = 100;
        uicontrol("Parent", f, ..
                  "Style", "text", ..
                  "String", "X-factor: ", ..
                  "Position", [0 figHeight-btnHeight labelWidth btnHeight], ..
                  "BackgroundColor", figbg, ..
                  "FontName", "Arial", ..
                  "FontWeight", "bold", ..
                  "FontSize", 12, ..
                  "Enable", "on");
        factNames = [];
        for kFact = 1:size(sopProj.factors)
            factNames($+1) = sopProj.factors(kFact).name;
        end
        uicontrol("Parent", f, ..
                  "Style", "popupmenu", ..
                  "String", factNames, ..
                  "Position", [labelWidth figHeight-btnHeight popupWidth btnHeight], ..
                  "BackgroundColor", btnbg, ..
                  "FontName", "Arial", ..
                  "FontWeight", "bold", ..
                  "FontSize", 12, ..
                  "Enable", "on", ..
                  "Value", indaxeX, ...
                  "Callback", "sopGraphReponse2D",..
                  "Tag", "xfactor");
        uicontrol("Parent", f, ..
                  "Style", "text", ..
                  "String", "Y-factor: ", ..
                  "Position", [labelWidth+popupWidth+margin figHeight-btnHeight labelWidth btnHeight], ..
                  "BackgroundColor", figbg, ..
                  "FontName", "Arial", ..
                  "FontWeight", "bold", ..
                  "FontSize", 12, ..
                  "Enable", "on");
        uicontrol("Parent", f, ..
                  "Style", "popupmenu", ..
                  "String", factNames, ..
                  "Position", [2*labelWidth+popupWidth+margin figHeight-btnHeight popupWidth btnHeight], ..
                  "BackgroundColor", btnbg, ..
                  "FontName", "Arial", ..
                  "FontWeight", "bold", ..
                  "FontSize", 12, ..
                  "Enable", "on", ..
                  "Value", indaxeY, ...
                  "Callback", "sopGraphReponse2D",..
                  "Tag", "yfactor");
        uicontrol("Parent", f, ..
                  "Style", "text", ..
                  "String", "Response: ", ..
                  "Position", [2*labelWidth+2*popupWidth+2*margin figHeight-btnHeight labelWidth btnHeight], ..
                  "BackgroundColor", figbg, ..
                  "FontName", "Arial", ..
                  "FontWeight", "bold", ..
                  "FontSize", 12, ..
                  "Enable", "on");
        respNames = [];
        for kResp = 1:size(sopProj.responses)
            respNames($+1) = sopProj.responses(kResp).name;
        end
        uicontrol("Parent", f, ..
                  "Style", "popupmenu", ..
                  "String", respNames, ..
                  "Position", [3*labelWidth+2*popupWidth+2*margin figHeight-btnHeight popupWidth btnHeight], ..
                  "BackgroundColor", btnbg, ..
                  "FontName", "Arial", ..
                  "FontWeight", "bold", ..
                  "FontSize", 12, ..
                  "Enable", "on", ..
                  "Callback", "sopGraphReponse2D",..
                  "Value", responseIndex, ...
                  "Tag", "responseindex");
    else
        f = findobj("tag", "2Drespvisu");
        g = gdf();
        f.color_map = g.color_map;
        figbg = sopGuiParameters("FigureBackground");
        set(f, "background", addcolor(figbg));
        responseIndex = get(findobj("Tag", "responseindex"), "value");
        indaxeX = get(findobj("Tag", "xfactor"), "value");
        indaxeY = get(findobj("Tag", "yfactor"), "value");
        
        drawlater();
        // Remove axes used for previous display
        while %T
            if f.children(1).type == "Axes" then
                delete(f.children(1))
            else
                break;
            end
        end
        drawnow();
    end


    // Reference point (center point if not user defined)
    fullDoe = sopGetDoe(sopProj, responseIndex, "goodpoints");
    refpoint = [];
    for currentFactorIndex=1:size(sopProj.factors)
        mini = min(fullDoe(:, currentFactorIndex), "r");
        maxi = max(fullDoe(:, currentFactorIndex), "r");
        refpoint(1, currentFactorIndex) = mini + (maxi-mini)/2;
    end

    nbpointspardim = 20;
    minX = min(fullDoe(:,indaxeX), "r");
    maxX = max(fullDoe(:,indaxeX), "r");
    dX = (maxX-minX)/(nbpointspardim-1);
    minY = min(fullDoe(:,indaxeY), "r");
    maxY = max(fullDoe(:,indaxeY), "r");
    dY = (maxY-minY)/(nbpointspardim-1);
    [mgx,mgy] = meshgrid(minX:dX:maxX,minY:dY:maxY);

    plan = repmat(refpoint,nbpointspardim*nbpointspardim,1);
    plan(:,indaxeX) = mgx(:);
    plan(:,indaxeY) = mgy(:);

    drawlater();

    xmin = %nan;
    ymin = %nan;
    xmax = %nan;
    ymax = %nan;

    // TODO Manage one response at once and one model per page
    for currentRespIndex=responseIndex;1:1//size(sopProj.responses)

        // Figure/axes management
        scf(f)
        a = sopSubplot(gca(), 1, 1, 1); //TODO make more flexible subplot
        if ~isempty(a.children) then
            delete(a.children);
        end
        sca(a);
        a.auto_clear = "off";
        a.grid = [1 1];
        a.box= "on";
        a.tight_limits="on";

        // Only possible for one model?
        for kModel = 1:1//size(sopProj.responses(responseIndex).models)
            allResponses = sopProj.responses#;
            allModels = allResponses(responseIndex).models#;
            mdl = sopGetValue(allModels(kModel));
            mdl.points = plan;
            allModels(kModel) = mdl;
            sopPrediction(allModels(kModel));
            predictedResponse = allModels(kModel).predictions
            
            //allModels(kModel).points = refpoint;
            //sopPrediction(allModels(kModel));
            //predictedRespRefPoint = allModels(kModel).predictions
        end

        // Manage Z data and avoid division by zero
        mgz=matrix(predictedResponse,size(mgx));
        if min(mgz)==max(mgz) then
            mgz(1)=mgz(1)+%eps;
        end

        // Draw contours
        // TODO: better management of constant responses
        nbContours = 10;
        l = min(mgz):(max(mgz)-min(mgz))/(nbContours+1):max(mgz)
        contourf(minX:dX:maxX,minY:dY:maxY,mgz',l)

        // Colormap management
        jcmapSize = 64;
        cmapSize = size(f.color_map, 1);
        jcmap = jetcolormap(jcmapSize);
        addcolor(jcmap)

        // Change colors of contours to match jetcolormap
        nbPolylines = size(l, "*");
        for kChild = nbPolylines:-1:1
            step = ceil(jcmapSize:-(jcmapSize/nbPolylines):1);
            a.children(kChild).foreground = -1; // Lines color (black)
            a.children(kChild).background = addcolor(jcmap(step(kChild),:)); // Fill color

            // Get min/max values to fit axes at the end of the drawing
            xymin = min(a.children(kChild).data, "r");
            xymax = max(a.children(kChild).data, "r");
            xmin = min([xmin,xymin(1)])
            ymin = min([ymin,xymin(2)])
            xmax = max([xmax,xymax(1)])
            ymax = max([ymax,xymax(2)])
        end

        // Adapt axes bounds to data
        if ~isempty(l) then
            a.data_bounds = [xmin ymin;xmax ymax]
        end
        sopSetAxesLabels(a, sopProj.factors(indaxeX), sopProj.factors(indaxeY));

        // Colorbar management (Current response value scale)
        colorbar(min(mgz), max(mgz),[cmapSize+1 cmapSize+jcmapSize])
        sopSetAxesLabels(f.children(1), "", "", sopProj.responses(currentRespIndex));
    end

    drawnow()
    a.tight_limits = "off"; // To avoid rendering problems in 5.4.0/1
endfunction

