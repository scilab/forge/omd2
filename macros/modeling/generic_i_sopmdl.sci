// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function modelObj = generic_i_sopmdl(propertyName, propertyValue, modelObj)

    if typeof(modelObj)<>"sopmdl" then
        error(msprintf("%s: Wrong type for input argument #%d: A ''%s'' tlist expected.\n", "generic_i_sopmdl", 3, "sopmdl"));
    end
    if typeof(propertyName)<>"string" then
        error(msprintf("%s: Wrong type for input argument #%d: A string expected.\n", "generic_i_sopmdl", 1));
    end
    if size(propertyName, "*")<>1 then
        error(msprintf("%s: Wrong size for input argument #%d: A string expected.\n", "generic_i_sopmdl", 1));
    end

    propertyName = "<" + propertyName + ">";

    select propertyName
    case "<coeffs>"
        // TODO is type checking possible here?
        if typeof(propertyValue) == "dace_model" then
            propertyValue.regr = get_param(modelObj.params, "regr")
            propertyValue.corr = get_param(modelObj.params, "_corr")
        end
        modelObj(propertyName) = propertyValue;
    case "<funname>"
        if typeof(propertyValue)<>"string" then
            error(msprintf("Wrong type for ''%s'' property: A string expected.\n", "Funname"));
        end
        if size(propertyName, "*")<>1 then
            error(msprintf("Wrong size for ''%s'' property: A string expected.\n", "Funname"));
        end
        modelObj(propertyName) = propertyValue;
    case "<params>"
        if typeof(propertyValue)<>"plist" then
            error(msprintf("Wrong type for ''%s'' property: A ''plist'' tlist expected.\n", "params"));
        end
        modelObj(propertyName) = propertyValue;
    case "<points>"
        // TODO type and size checks
        modelObj(propertyName) = propertyValue;
     case "<predictions>"
        // TODO type and size checks
        modelObj(propertyName) = propertyValue;
    case "<version>"
        if typeof(propertyValue)<>"constant" then
            error(msprintf("Wrong type for ''%s'' property: A %d-by-%d real vector expected.\n", "Version", 1, 3));
        end
        if or(size(propertyValue)<>[1 3]) then
            error(msprintf("Wrong size for ''%s'' property: A %d-by-%d real vector expected.\n", "Version", 1, 3));
        end
        modelObj(propertyName) = propertyValue;
    else
        error(msprintf("''%s'' is not a valid property name.\n", propertyName));
    end
endfunction

