// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopPointsSelectionGui(responseIndex)

    h = findobj("Tag", "soppointsselectiongui");
    if ~isempty(h) then
        delete(h)
    end

    sopSetGuiEnable(findobj("tag", "sopmodelinggui"), "off");

    [figbg, margin, listboxbg, btnbg, btnh] = sopGuiParameters("FigureBackground", ..
                                                               "Margin", ..
                                                               "ListboxBackground", ..
                                                               "ButtonBackground", ..
                                                               "ButtonHeight");
    
    // Add information on modeling GUI
    sopModGui = findobj("Tag", "sopmodelinggui");
    uicontrol("Parent", sopModGui, ..
              "Style", "text", ..
              "Position", [sopModGui.position(3)/2-100 sopModGui.position(4)/2-20 200 40], ..
              "BackgroundColor", figbg, ..
              "FontWeight", "bold", ..
              "String", _("Points selection..."), ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", "on", ..
              "Userdata", [], ..
              "Tag", "waitingforpointsselgui");
    // This object will be used to retrieve modified parameters and set them to the right project field
    // userdata = list(objectTag, newParameters)
    // newParameters = [] if the user cancels
    
    sopproject = sopGetCurrentProject(sopModGui);

    figw = 600 + 5*margin + 40;
    figh = 11*margin + 6*btnh;
    h = sopFigure("", [100 100 figw figh], "soppointsselectiongui", sopproject, %T, %T)
    h.closerequestfcn = "cbSopPointsSelectionGui(""cancel_btn"")";
    h.resizefcn = "rsSopPointsSelectionGui();";

    // Add a menu to select currently edited
    respMenu = uimenu("Parent", h, ..
                      "Label", _("Responses"), ..
                      "Tag", "responses_menu");
    projectResponses = sopproject.responses;
    for kResp = 1:size(projectResponses)
        mnu = uimenu("Parent", respMenu, ..
                     "Label", projectResponses(kResp).name, ..
                     "Callback", "cbSopPointsSelectionGui();", ..
                     "Tag", "currentresponse" + string(kResp) + "_menu");
        if kResp == responseIndex then
            mnu.checked = "on";
        else
            mnu.checked = "off"
        end
    end
    
    // Add validation button
    btnw = 60;
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "Position", [figw-margin-btnw margin btnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "String", _("Ok"), ..
              "Callback", "cbSopPointsSelectionGui();", ..
              "Tag", "ok_btn");
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "Position", [figw-2*margin-2*btnw margin btnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "String", _("Cancel"), ..
              "Callback", "cbSopPointsSelectionGui();", ..
              "Tag", "cancel_btn");

    // Points list frame
    framex = margin;
    framey = 2*margin + btnh;
    framew = 150;
    frameh = figh-3*margin-btnh;
    sopFrameWithTitle(h, [framex, framey, framew, frameh], "Points", 40, "points");
    // Add listbox
    uicontrol("Parent", h, ..
              "Style", "listbox", ..
              "Position", [framex+margin framey+margin framew-2*margin frameh-2*margin], ..
              "BackgroundColor", listboxbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Value", 1, ..
              "Enable", "on", ..
              "Callback", "cbSopPointsSelectionGui();", ..
              "Tag", "points_listbox");

    // Points properties frame
    framex = 2*margin + framew;
    framey = 2*margin + btnh;
    framew = 150;
    frameh = 4*btnh + 5*margin;
    sopFrameWithTitle(h, [framex, framey, framew, frameh], "Properties", 60, "properties");
    uicontrol("Parent", h, ..
              "Style", "checkbox", ..
              "Position", [framex+margin framey+margin framew-2*margin btnh], ..
              "BackgroundColor", figbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Max", 1, ..
              "Min", 0, ..
              "String", _("DOE center"), ..
              "Callback", "cbSopPointsSelectionGui();", ..
              "Tag", "doecenter_checkbox");
    uicontrol("Parent", h, ..
              "Style", "radiobutton", ..
              "Position", [framex+margin framey+2*margin+btnh framew-2*margin btnh], ..
              "BackgroundColor", figbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "String", _("Bad point"), ..
              "Callback", "cbSopPointsSelectionGui();", ..
              "Tag", "badpoint_checkbox");
    uicontrol("Parent", h, ..
              "Style", "radiobutton", ..
              "Position", [framex+margin framey+3*margin+2*btnh framew-2*margin btnh], ..
              "BackgroundColor", figbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "String", _("Validation point"), ..
              "Callback", "cbSopPointsSelectionGui();", ..
              "Tag", "validationpoint_checkbox");
    uicontrol("Parent", h, ..
              "Style", "radiobutton", ..
              "Position", [framex+margin framey+4*margin+3*btnh framew-2*margin btnh], ..
              "BackgroundColor", figbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "String", _("Learning point"), ..
              "Callback", "cbSopPointsSelectionGui();", ..
              "Tag", "learningpoint_checkbox");

    // Plex numerotation frame
    framex = 2*margin + framew;
    framey = 3*margin + btnh + frameh;
    framew = 150;
    frameh = 2*margin + btnh;
    sopFrameWithTitle(h, [framex, framey, framew, frameh], "DOE numerotation", 90, "plexnumerotation");
    uicontrol("Parent", h, ..
              "Style", "popupmenu", ..
              "Position", [framex+margin framey+margin framew-2*margin btnh], ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Callback", "cbSopPointsSelectionGui();", ..
              "Tag", "doenumerotation_popup");

    // Graph
    selPointsAxes = gca();
    selPointsAxes.axes_bounds = [0.5 0 0.5 0.85];
    selPointsAxes.userdata = "selpoints_axes";
    selPointsAxes.background = -2;
    selPointsAxes.grid = [1 1];
    selPointsAxes.auto_clear = "off";

    rsSopPointsSelectionGui();
    upSopPointsSelectionGui();

endfunction
