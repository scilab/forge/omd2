// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopFrameWithTitle(parentFigure, framePosition, frameTitle, titleWidth, commonTag)

    framex = framePosition(1);
    framey = framePosition(2);
    framew = framePosition(3);
    frameh = framePosition(4);
    
    [figbg, margin] = sopGuiParameters("FigureBackground", "Margin");

    uicontrol("Parent", parentFigure, ..
              "Style", "frame", ..
              "Position", [framex framey framew frameh], ..
              "BackgroundColor", figbg, ..
              "Tag", commonTag + "_frame")

    uicontrol("Parent", parentFigure, ..
              "Style", "text", ..
              "String", frameTitle, ..
              "Position", [framex+margin framey+frameh-7 titleWidth 14], ..
              "BackgroundColor", figbg, ..
              "FontName", "Arial", ..
              "FontSize", 11, ..
              "Tag", commonTag + "_title")

endfunction

