// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopAbout()
    messagebox(_("This platform has been developped during OMD2 and CSDL project.<br><br>Main developer: Vincent COUVERT (DIGITEO - Scilab Consortium)"), ..
               _("About Scilab Optimization Platform"), "info", "modal")
endfunction

