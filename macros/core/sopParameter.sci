// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function parameterObj = sopParameter(paramName, paramType, paramValuesContraints, paramDefault)

    // Create the tlist and initialize fields with default values
    parameterObj = tlist(["sopparam", "_name", "_type", "_valuescontraints", "_defaultvalue", "_uservalue"]);

    // Set default values
    parameterObj.name = "";
    parameterObj.type = "";
    parameterObj.valuescontraints = [];
    parameterObj.defaultvalue = [];
    parameterObj.uservalue =  [];

    rhs = argn(2);
    if rhs >=1 then
        parameterObj.name = paramName;
    end
    if rhs >=2 then
        parameterObj.type = paramType;
    end
    if rhs >=3 then
        parameterObj.valuescontraints = paramValuesContraints;
        if rhs==3 then // Default value not given
            parameterObj.defaultvalue = paramValuesContraints(1);
        end
    end
    if rhs >=4 then
        parameterObj.defaultvalue = paramDefault;
    end
endfunction
