// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopSetCurrentProject(figHandle, sopObj)

    // TODO input size and type check
    rhs = argn(2);
    if rhs<>2 then
        error(msprintf(_("%s: Wrong number of input arguments: %d expected.\n"), "sopSetCurrentProject", 2))
    end

    if typeof(figHandle)<>"handle" then
        error(msprintf(_("%s: Wrong type for input argument #%d: A graphic handle expected.\n"), "sopSetCurrentProject", 1))
    end
    if size(figHandle, "*")<>1 then
        error(msprintf(_("%s: Wrong size for input argument #%d: A graphic handle expected.\n"), "sopSetCurrentProject", 1))
    end

    if sopObjectType(sopObj)<>"Project" then
        error(msprintf(_("%s: Wrong type for input argument #%d: A SOP object of type ''%s'' expected.\n"), "sopSetCurrentProject", 1, "Project"))
    end

    ud = figHandle.userdata;

    if typeof(ud)<>"st" then
        ud = struct();
    end

    ud.sopcurrentproject = sopObj;

    figHandle.userdata = ud;

    if isfield(ud, "sopupdatefunction") & ~isempty(ud.sopupdatefunction) then
        execstr(ud.sopupdatefunction);
    end

endfunction