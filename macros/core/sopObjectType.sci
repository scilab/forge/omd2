// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012 - DIGITEO - Vincent COUVERT
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopObjType = sopObjectType(sopObj)

    sopObjType = "unknown";

    if typeof(sopObj)<>"sopobj" then // Not a SOP object
        return
    end

    // Try to load corresponding data file
    try
        load(sopObj.workfile);
    catch
        return;
    end

    // Get the object data
    [obj, ierr] = evstr("sopproj" + sopObj.editeddata);

    if ierr<>0 then
        return
    end

    select typeof(obj)
    case "sopproj"
        sopObjType = "Project";
    case "sopdoege"
        sopObjType = "DoeGenerator";
    case "sopsimul"
        sopObjType = "Simulator";
    case "sopoptim"
        sopObjType = "Optimizer";
    case "sopmdl"
        sopObjType = "Model";
    case "sopvar"
        sopObjType = "Variable";
    case "list"
        sopObjType = "List of " + typeof(obj(1));
    end

endfunction
