// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function strArgOut = sopArgsOutFromParams(wrapperOutParams)
    
    strArgOut = "[";
    for kParam = 1:size(wrapperOutParams)
        strArgOut = strArgOut + "argOut" + string(kParam);
        if kParam<>size(wrapperOutParams) then
            strArgOut = strArgOut + ", ";
        end
    end
    strArgOut = strArgOut + "]";

endfunction