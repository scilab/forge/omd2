// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012 - Scilab Enterprises - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function objectHandle = %c_i_sophandl(propertyName, propertyValue, objectHandle)
    if propertyName<>"identifier" then
        sopDataController("setproperty", objectHandle, propertyName, propertyValue);
    else
        error("Identifier proprty is read-only.")
    end
endfunction
