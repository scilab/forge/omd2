// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012 - DIGITEO - Vincent COUVERT
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopObj = generic_i_sopobj(propertyName, propertyValue, sopObj)

    if typeof(propertyName)<>"string" then
        if typeof(propertyName)<>"polynomial" & typeof(propertyName)<>"constant" then
            error(msprintf("%s: Wrong type for input argument #%d: A polynomial or a constant expected.\n", "generic_i_sopobj", 1));
        end
        load(sopObj.workfile)
        if ~isempty(sopObj.editeddata) then
            pointedData = evstr("sopproj" + sopObj.editeddata)
            pointedData(propertyName) = propertyValue;
            execstr("sopproj" + sopObj.editeddata + "= pointedData;")
        else
            sopproj(propertyName) = propertyValue
        end
        save(sopObj.workfile, "sopproj")
        return
    end


    if typeof(sopObj)<>"sopobj" then
        error(msprintf("%s: Wrong type for input argument #%d: A ''%s'' tlist expected.\n", "generic_i_sopobj", 3, "sopobj"));
    end
    if typeof(propertyName)<>"string" then
        error(msprintf("%s: Wrong type for input argument #%d: A string expected.\n", "generic_i_sopobj", 1));
    end
    if size(propertyName, "*")<>1 then
        error(msprintf("%s: Wrong size for input argument #%d: A string expected.\n", "generic_i_sopobj", 1));
    end

    shortPropertyName = propertyName;
    propertyName = "<" + propertyName + ">";

    select convstr(propertyName, "l")
    case "<creationdate>"
        sopObj(propertyName) = propertyValue;
    case "<modificationdate>"
        sopObj(propertyName) = propertyValue;
    case "<pointedfile>"
        if typeof(propertyValue)<>"string" then
            error(msprintf("Wrong type for ''%s'' property: A string expected.\n", "PointedFile"));
        end
        if size(propertyValue, "*")<>1 then
            error(msprintf("Wrong size for ''%s'' property: A string expected.\n", "PointedFile"));
        end
        sopObj(propertyName) = propertyValue;
    case "<origfile>"
        if typeof(propertyValue)<>"string" then
            error(msprintf("Wrong type for ''%s'' property: A string or a figure handle expected.\n", "OrigFile"));
        end
        if size(propertyValue, "*")<>1 then
            error(msprintf("Wrong size for ''%s'' property: A string or a figure handle expected.\n", "OrigFile"));
        end
        sopObj(propertyName) = propertyValue;
    case "<workfile>"
        if typeof(propertyValue)<>"string" then
            error(msprintf("Wrong type for ''%s'' property: A string or a figure handle expected.\n", "WorkFile"));
        end
        if size(propertyValue, "*")<>1 then
            error(msprintf("Wrong size for ''%s'' property: A string or a figure handle expected.\n", "WorkFile"));
        end
        sopObj(propertyName) = propertyValue;
    case "<editeddata>"
        if typeof(propertyValue)<>"string" then
            error(msprintf("Wrong type for ''%s'' property: A string expected.\n", "EditedData"));
        end
        if size(propertyValue, "*")<>1 then
            error(msprintf("Wrong size for ''%s'' property: A string expected.\n", "EditedData"));
        end
        sopObj(propertyName) = propertyValue;
    else
        // Insertion in the pointed data
        load(sopObj.workfile)
        if ~isempty(sopObj.editeddata) then
            pointedData = evstr("sopproj" + sopObj.editeddata)
            if typeof(pointedData) == "plist" then
                if is_param(pointedData, shortPropertyName) then
                    pointedData = set_param(pointedData, shortPropertyName, propertyValue);
                else
                    pointedData = add_param(pointedData, shortPropertyName, propertyValue);
                end
            else
                pointedData(propertyName) = propertyValue;
            end
            execstr("sopproj" + sopObj.editeddata + "= pointedData;")
        else
            sopproj(shortPropertyName) = propertyValue
        end
        save(sopObj.workfile, "sopproj")
    end
endfunction
