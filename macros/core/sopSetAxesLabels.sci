// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopSetAxesLabels(currentAxes, xlabelString, ylabelString, titleString)

    curAxesSave = gca();
    sca(currentAxes);

    if ~isempty(xlabelString) then
        if typeof(xlabelString) == "sopvar" then
            if isempty(xlabelString.units) then
                xlabelString = xlabelString.name;
            else
                xlabelString = xlabelString.name + "(" + xlabelString.units + ")";
            end
        end
        currentAxes.x_label.text = xlabelString;
        currentAxes.x_label.font_style = 8;
    end

    if argn(2)>2 & ~isempty(ylabelString) then
        if typeof(ylabelString) == "sopvar" then
            if isempty(ylabelString.units) then
                ylabelString = ylabelString.name;
            else
                ylabelString = ylabelString.name + "(" + ylabelString.units + ")";
            end
        end
        currentAxes.y_label.text = ylabelString;
        currentAxes.y_label.font_style = 8;
    end

    if argn(2)>3 & ~isempty(titleString) then
        if typeof(titleString) == "sopvar" then
            if isempty(titleString.units) then
                titleString = titleString.name;
            else
                titleString = titleString.name + "(" + titleString.units + ")";
            end
        end
        currentAxes.title.text = titleString;
        currentAxes.title.font_style = 8;
    end

    sca(curAxesSave);

endfunction
