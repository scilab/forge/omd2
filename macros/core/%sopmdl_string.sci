// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2013 - Scilab Enterprises - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function t = %sopmdl_string(mdlObj)
    t      = "Version: " + %sopversion_string(mdlObj.version);
    t($+1) = "FunName: " + mdlObj.funname;
    t($+1) = "Params: [" + string(lstsize(mdlObj.params)) + " parameters]";
    t($+1) = "Coeffs: " + string(mdlObj.coeffs);
    t($+1) = "Points: [" + string(size(mdlObj.points, 1)) + " points]";
    t($+1) = "Predictions [" + string(size(mdlObj.predictions, 1)) + " values]";
endfunction
