// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2013 - Scilab Enterprises - Vincent COUVERT
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function resizeSopGui()
    fig = findobj("Tag", "sopgui");
    hdls = sopGuiHandles(fig);
    
    txtHeight = 40;
    figh = fig.position(4);
    figw = fig.position(3);
    btnWidth = figw/3;
    txtWidth = figw/3;
    btnHeight = figh-2*txtHeight;
    
    hdls.databtn.position = [0 2*txtHeight btnWidth btnHeight];
    hdls.dataconfigstatustxt.position = [0 txtHeight txtWidth txtHeight];
    hdls.dataexecstatustxt.position = [0 0 txtWidth txtHeight];

    hdls.modelingbtn.position = [btnWidth 2*txtHeight btnWidth btnHeight];
    hdls.modelingconfigstatustxt.position = [btnWidth txtHeight txtWidth txtHeight];
    hdls.modelingexecstatustxt.position = [btnWidth 0 txtWidth txtHeight];

    hdls.optimbtn.position = [2*btnWidth 2*txtHeight btnWidth btnHeight];
    hdls.optimconfigstatustxt.position = [2*btnWidth txtHeight txtWidth txtHeight];
    hdls.optimexecstatustxt.position = [2*btnWidth 0 txtWidth txtHeight];
endfunction

