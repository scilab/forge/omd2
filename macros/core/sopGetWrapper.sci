// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function wrapper = sopGetWrapper(wrapperType, wrapperName)
    
    rhs = argn(2);
    if rhs<1 | rhs>2 then
        error(msprintf(gettext("%s: Wrong number of input arguments: %d or %d expected.\n"), "sopGetWrapper", 1, 2))
    end
    if typeof(wrapperType)<>"string" then
        error(msprintf("%s: Wrong type for input argument #%d: A string expected.\n", "sopGetWrapper", 1));
    end
    if size(wrapperType, "*")<>1 then
        error(msprintf("%s: Wrong size for input argument #%d: A string expected.\n", "sopGetWrapper", 1));
    end
    
    if rhs==1 then // Get all wrapper of a type
        wrapperName = [];
    else
        if typeof(wrapperName)<>"string" then
            error(msprintf("%s: Wrong type for input argument #%d: A string expected.\n", "sopGetWrapper", 2));
        end
        if size(wrapperName, "*")<>1 then
            error(msprintf("%s: Wrong size for input argument #%d: A string expected.\n", "sopGetWrapper", 2));
        end
    end

    wrapper = [];

    select convstr(wrapperType, "l")
    case "doegenerator"
        wrapper = sopLoadWrappers(".sdf", "sopdoegenerator", wrapperName);
    case "modeler"
        wrapper = sopLoadWrappers(".smf", "sopmodeler", wrapperName);
    case "simulator"
        wrapper = sopLoadWrappers(".ssf", "sopsimulator", wrapperName);
    case "optimizer"
        wrapper = sopLoadWrappers(".sof", "sopoptimizer", wrapperName);
    end
    
    if isempty(wrapper) then
        sopError(msprintf(gettext("Wrapper of type ''%s'' with name ''%s'' not found."), wrapperType, wrapperName));
    end


endfunction

function wrapper = sopLoadWrappers(wrappersExtension, variableName, wrapperName)

    wrapper = [];

    // Load all wrappers
    wrappersPaths = sopModulePath() + filesep() + "examples/";
    wrappersList = list();
    for kPath = 1: size(wrappersPaths, "*")
        allWrappersFiles = ls(pathconvert(wrappersPaths(kPath)) + "*" + wrappersExtension);
        for kFile = 1:size(allWrappersFiles, "*")
            execstr(variableName + " = [];")
            import_from_hdf5(allWrappersFiles(kFile));
            if isempty(evstr(variableName)) then
                sopError(msprintf(gettext("Could not load wrapper from ''%s''\n."), allWrappersFiles(kFile)));
            else
                wrappersList($+1) = evstr(variableName);
            end
        end
    end

    if isempty(wrapperName) then // Return all wrappers
        wrapper = wrappersList;
    else
        // Find searched wrapper
        for kWrapper = 1:size(wrappersList)
            if wrappersList(kWrapper).name==wrapperName then
                wrapper = wrappersList(kWrapper);
                break;
            end
        end
    end
endfunction
