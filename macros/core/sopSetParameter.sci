// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopSetParameter(obj, paramName, paramValue)

    // obj is a generator, a model, an optimizer,...
    
    params = obj.params;
    if is_param(params, paramName) then
        params = set_param(params, paramName, paramValue);
    else
        params = add_param(params, paramName, paramValue);
    end
    obj.params = params;

endfunction
