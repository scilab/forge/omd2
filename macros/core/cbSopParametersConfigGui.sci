// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function cbSopParametersConfigGui()

    h = findobj("Tag", "sopparametersconfiggui");
    ud = get(h, "Userdata")

    gcboTag = get(gcbo, "Tag");
    ud = ud.sopparametersconfig;
    wrapperParameters = ud(1);
    editedObject = ud(2);
    parentFigureHandle = ud(3);
    paramsField = ud(4);

    // Get the parameters values
    objectParameters = editedObject(paramsField);

    // Manage OK button status
    // Will not be enabled if data are not valid
    okEnable = "on";
    for kParam = 1:size(wrapperParameters)
        select wrapperParameters(kParam).type
        case "Filename"
            pushBtn = findobj("Tag", wrapperParameters(kParam).name);
            if and(gcboTag==wrapperParameters(kParam).name) then
                // Open a file selection dialog
                [fileName, pathName] = uigetfile("*.sci", pwd(), _("Select the model file"));
                if ~isempty(fileName) & ~isempty(pathName) then
                    set(pushBtn, "String", fileparts(pathName + filesep() + fileName, "fname"));
                    set(pushBtn, "Userdata", pathName + filesep() + fileName);
                end
            end    
            // Try to exec the file to be sure it is a Scilab function
            ierr = execstr("exec(get(pushBtn, ""Userdata""), -1);", "errcatch");
            if ierr<>0 then
                set(pushBtn, "BackgroundColor", [1 0 0]);
                okEnable = "off";
            else
                okEnable = "on";
                set(pushBtn, "BackgroundColor", [1 1 1]);
            end
        case "FunctionName"
            // The function must exists
            editBox = findobj("Tag", wrapperParameters(kParam).name);
            [editValue, errStatus] = evstr(get(editBox, "String"));
            if exists(get(editBox, "String"))==0 | typeof(editValue)<>"function" then
                set(editBox, "BackgroundColor", [1 0 0]);
                okEnable = "off";
            else
                okEnable = "on";
                set(editBox, "BackgroundColor", [1 1 1]);
            end
        case "Constant"
            // Check bounds
            bounds = wrapperParameters(kParam).valuesconstraints;
            editBox = findobj("Tag", wrapperParameters(kParam).name);
            [editValue, errStatus] = evstr(get(editBox, "String"));
            if errStatus<>0 | editValue<bounds(1) | editValue<bounds(1) then
                set(editBox, "BackgroundColor", [1 0 0]);
                okEnable = "off";
            else
                set(editBox, "BackgroundColor", [1 1 1]);
                okEnable = "on";
            end
        case "String"
            // Nothing to do?
        case "Boolean"
            // Nothing to do because popupmenu
        else
            error("Unknown parameter type: " + string(wrapperParameters(kParam).type)); // TODO
        end
    end

    set(findobj("Tag", "ok_btn"), "Enable", okEnable);

    select gcboTag
    case "cancel_btn"
        delete(h);
        parentFigureHandle.info_message = "";
        execstr(parentFigureHandle.userdata.sopupdatefunction);
    case "ok_btn"
        if okEnable=="off" then // Focus lost by editbox when OK button was clicked
            return
        end

        // Update the initial parameters
        for kParam = 1:size(wrapperParameters)
            select wrapperParameters(kParam).type
            case "Filename"
                btnUd = get(findobj("Tag", wrapperParameters(kParam).name), "Userdata");
                objectParameters = set_param(objectParameters, wrapperParameters(kParam).name, btnUd);
            case "Function"
                popupStr = get(findobj("Tag", wrapperParameters(kParam).name), "String");
                popupValue = get(findobj("Tag", wrapperParameters(kParam).name), "Value");
                objectParameters = set_param(objectParameters, wrapperParameters(kParam).name, popupStr(popupValue));
            case "String"
                editBox = findobj("Tag", wrapperParameters(kParam).name);
                editValue= get(editBox, "String");
                objectParameters = set_param(objectParameters, wrapperParameters(kParam).name, editValue);
            case "Constant"
                // Check bounds
                bounds = wrapperParameters(kParam).valuesconstraints;
                editBox = findobj("Tag", wrapperParameters(kParam).name);
                [editValue, errStatus] = evstr(get(editBox, "String"));
                if errStatus<>0 | editValue<bounds(1) | editValue<bounds(1) then
                    set(editBox, "BackgroundColor", [1 0 0]);
                    okEnable = "off";
                else
                    set(editBox, "BackgroundColor", [1 1 1]);
                    okEnable = "on";
                end
                objectParameters = set_param(objectParameters, wrapperParameters(kParam).name, editValue);
            case "Boolean"
                popupValue = get(findobj("Tag", wrapperParameters(kParam).name), "Value");
                objectParameters = set_param(objectParameters, wrapperParameters(kParam).name, popupValue==2);
                // Nothing to do because popupmenu
            else
                error("Unknown parameter type: " + string(wrapperParameters(kParam).type)); // TODO
            end
        end
        editedObject(paramsField) = objectParameters;
        delete(h);
        parentFigureHandle.info_message = "";
        execstr(parentFigureHandle.userdata.sopupdatefunction);
    end
endfunction
