// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function projectObj = generic_i_sopproj(propertyName, propertyValue, projectObj)

    if typeof(projectObj)<>"sopproj" then
        error(msprintf("%s: Wrong type for input argument #%d: A ''%s'' tlist expected.\n", "generic_i_sopproj", 3, "sopproj"));
    end
    if typeof(propertyName)<>"string" then
        error(msprintf("%s: Wrong type for input argument #%d: A string expected.\n", "generic_i_sopproj", 1));
    end
    if size(propertyName, "*")<>1 then
        error(msprintf("%s: Wrong size for input argument #%d: A string expected.\n", "generic_i_sopproj", 1));
    end

    propertyName = "<" + convstr(propertyName, "l") + ">";

    select propertyName
    case "<dataconfigstatus>"
        if typeof(propertyValue)<>"sopstat" then
            error(msprintf("Wrong type for ''%s'' property: A ''%s'' tlist expected.\n", "DataConfigStatus", "sopstat"));
        end
        projectObj(propertyName) = propertyValue;
    case "<dataexecstatus>"
        if typeof(propertyValue)<>"sopstat" then
            error(msprintf("Wrong type for ''%s'' property: A ''%s'' tlist expected.\n", "DataExecStatus", "sopstat"));
        end
        projectObj(propertyName) = propertyValue;
    case "<datasource>"
        if typeof(propertyValue)<>"string" then
            error(msprintf("Wrong type for ''%s'' property: A string expected.\n", "DataSource"));
        end
        if size(propertyValue, "*")<>1 then
            error(msprintf("Wrong size for ''%s'' property: A string expected.\n", "DataSource"));
        end
        if and(propertyValue<>["doe", "file"]) then
            error(msprintf("Wrong value for ''%s'' property: ''%s'' or ''%s'' expected.\n", "DataSource", "file", "doe"));
        end
        projectObj(propertyName) = propertyValue;
    case "<doecenterpointindex>"
        if typeof(propertyValue)<>"constant" then
            error(msprintf("Wrong type for ''%s'' property: A real expected.\n", "DoeCenterPointIndex"));
        end
        if size(propertyValue, "*")<>1 then
            error(msprintf("Wrong size for ''%s'' property: A real expected.\n", "DoeCenterPointIndex"));
        end
        if propertyValue<0 then
            error(msprintf("Wrong value for ''%s'' property: Must be greater or equal to %d.\n", "DoeCenterPointIndex", 0));
        end
        projectObj(propertyName) = propertyValue;
    case "<doegenerators>"
        if typeof(propertyValue)<>"list" then
            error(msprintf("Wrong type for ''%s'' property: A list expected.\n", "DoeGenerators"));
        end
        for kGen = 1:size(propertyValue)
            if typeof(propertyValue(kGen))<>"sopdoege" then
                error(msprintf("%s: Wrong type for ''%s'' property (item #%d): A ''%s'' tlist expected.\n", "set", "DoeGenerators", kGen, "sopdoege"));
            end
        end
        projectObj(propertyName) = propertyValue;
    case "<doenumerotation>"
        // Must be a variable
        if ~isempty(propertyValue) & typeof(propertyValue)<>"sopvar" then
            error(msprintf("Wrong type for ''%s'' property: A ''%s'' tlist expected.\n", "DoeNumerotation", "sopvar"));
        end
        projectObj(propertyName) = propertyValue;
    case "<factors>"
        if typeof(propertyValue)<>"list" then
            error(msprintf("Wrong type for ''%s'' property: A list expected.\n", "Factors"));
        end
        for kData = 1:size(propertyValue)
            if typeof(propertyValue(kData))<>"sopvar" then
                error(msprintf("Wrong type for ''%s'' property (item #%d): A ''%s'' tlist expected.\n", "Factors", kData, "sopvar"));
            end
        end
        projectObj(propertyName) = propertyValue;
    case "<factorsfromvariables>"
        // Variables moved to factors
        variablesIndexes = propertyValue;
        for kVar = 1:length(variablesIndexes)
            theVar = projectObj.variables(variablesIndexes(kVar)); // TODO Scilab bug?: does not work directly here bug works for responses below...
            projectObj.factors($+1) = theVar;
            projectObj.variables(variablesIndexes(kVar)) = null();
            variablesIndexes = variablesIndexes - 1;
            if isempty(projectObj.factors($).valuestype) then // Do not erase if already set (when loading a validation point dataset for example)
                projectObj.factors($).valuestype = ones(size(projectObj.factors($).values, 1), 1);
            end
        end
    case "<filedata>"
        if typeof(propertyValue)<>"list" then
            error(msprintf("Wrong type for ''%s'' property: A list expected.\n", "FilaData", 3));
        end
        for kData = 1:size(propertyValue)
            if typeof(propertyValue(kData))<>"sopvar" then
                error(msprintf("Wrong type for ''%s'' property (item #%d): A ''%s'' tlist expected.\n", "Filedata", kData, "sopvar"));
            end
        end
        projectObj(propertyName) = propertyValue;
    case "<modelconfigstatus>"
        if typeof(propertyValue)<>"sopstat" then
            error(msprintf("Wrong type for ''%s'' property: A ''%s'' tlist expected.\n", "ModelConfigStatus", "sopstat"));
        end
        projectObj(propertyName) = propertyValue;
    case "<modelexecstatus>"
        if typeof(propertyValue)<>"sopstat" then
            error(msprintf("Wrong type for ''%s'' property: A ''%s'' tlist expected.\n", "ModelExecStatus", "sopstat"));
        end
        projectObj(propertyName) = propertyValue;
    case "<description>"
        if typeof(propertyValue)<>"string" then
            error(msprintf("Wrong type for ''%s'' property: A string expected.\n", "Description"));
        end
        if size(propertyValue, "*")<>1 then
            error(msprintf("%s: Wrong size for ''%s'' property: A real expected.\n", "Description"));
        end
        projectObj(propertyName) = propertyValue;
    case "<paparams>"
        // TODO type/size check
        projectObj(propertyName) = propertyValue;
    case "<optimalpoint>"
        // TODO type/size check
        projectObj(propertyName) = propertyValue;
    case "<optimconfigstatus>"
        if typeof(propertyValue)<>"sopstat" then
            error(msprintf("Wrong type for ''%s'' property: A ''%s'' tlist expected.\n", "OptimConfigStatus", "sopstat"));
        end
        projectObj(propertyName) = propertyValue;
    case "<optimexecstatus>"
        if typeof(propertyValue)<>"sopstat" then
            error(msprintf("Wrong type for ''%s'' property: A ''%s'' tlist expected.\n", "OptimExecStatus", "sopstat"));
        end
        projectObj(propertyName) = propertyValue;
    case "<objfunctionoptimalvalue>"
        // TODO type/size check
        projectObj(propertyName) = propertyValue;
    case "<optimizers>"
        if typeof(propertyValue)<>"list" then
            error(msprintf("Wrong type for ''%s'' property: A list expected.\n", "Optimizers"));
        end
        for kOpt = 1:size(propertyValue)
            if typeof(propertyValue(kOpt))<>"sopoptim" then
                error(msprintf("%s: Wrong type for ''%s'' property (item #%d): A ''%s'' tlist expected.\n", "set", kOpt, "sopoptim"));
            end
        end
        projectObj(propertyName) = propertyValue;
    case "<responses>"
        if typeof(propertyValue)<>"list" then
            error(msprintf("Wrong type for ''%s'' property: A list expected.\n", "Responses"));
        end
        for kData = 1:size(propertyValue)
            if typeof(propertyValue(kData))<>"sopvar" then
                error(msprintf("Wrong type for ''%s'' property (item #%d): A ''%s'' tlist expected.\n", "Responses", kData, "sopvar"));
            end
        end
        projectObj(propertyName) = propertyValue;
    case "<responsescoeffs>"
        // TODO type checking
        projectObj(propertyName) = propertyValue;
    case "<responsesfromvariables>"
        variablesIndexes = propertyValue;
        for kVar = 1:length(variablesIndexes)
            projectObj.responses($+1) = projectObj.variables(variablesIndexes(kVar));
            projectObj.variables(variablesIndexes(kVar)) = null();
            variablesIndexes = variablesIndexes - 1;
            if isempty(projectObj.responses($).valuestype) then // Do not erase if already set (when loading a validation point dataset for example)
                projectObj.responses($).valuestype = ones(size(projectObj.responses($).values, 1), 1);
            end
        end
    case "<simulator>"
        if typeof(propertyValue)<>"sopsimul" then
            error(msprintf("Wrong type for ''%s'' property: A ''sopsimul'' tlist expected.\n", "Simulator"));
        end
        projectObj(propertyName) = propertyValue;
    case "<variables>"
        if typeof(propertyValue)<>"list" then
            error(msprintf("Wrong type for ''%s'' property: A list expected.\n", "Variables"));
        end
        for kData = 1:size(propertyValue)
            if typeof(propertyValue(kData))<>"sopvar" then
                error(msprintf("Wrong type for ''%s'' property (item #%d): A ''%s'' tlist expected.\n", "Variables", kData, "sopvar"));
            end
        end
        projectObj(propertyName) = propertyValue;
    case "<variablesfromfactors>"
        factorsIndexes = propertyValue;
        for kFact = 1:length(factorsIndexes)
            newVariables = list();
            indexinfile = projectObj.factors(factorsIndexes(kFact)).indexinfile;

            kVar = 1;
            while kVar<=size(projectObj.variables) & projectObj.variables(kVar).indexinfile < indexinfile
                newVariables($+1) = projectObj.variables(kVar);
                kVar = kVar + 1;
            end

            newVariables($+1) = projectObj.factors(factorsIndexes(kFact));
            projectObj.factors(factorsIndexes(kFact)) = null();

            while kVar<=size(sopProj.variables)
                newVariables($+1) = projectObj.variables(kVar);
                kVar = kVar + 1;
            end
            factorsIndexes = factorsIndexes - 1;
            projectObj("<variables>") = newVariables;
        end
    case "<variablesfromresponses>"
        responsesIndexes = propertyValue;
        for kResp = 1:length(responsesIndexes)
            newVariables = list();
            indexinfile = projectObj.responses(responsesIndexes(kResp)).indexinfile;

            kVar = 1;
            while kVar<=size(projectObj.variables) & projectObj.variables(kVar).indexinfile < indexinfile
                newVariables($+1) = projectObj.variables(kVar);
                kVar = kVar + 1;
            end

            newVariables($+1) = projectObj.responses(responsesIndexes(kResp));
            projectObj.responses(responsesIndexes(kResp)) = null();

            while kVar<=size(projectObj.variables)
                newVariables($+1) = projectObj.variables(kVar);
                kVar = kVar + 1;
            end
            responsesIndexes = responsesIndexes - 1;
            projectObj("<variables>") = newVariables;
        end
    case "<version>"
        if typeof(propertyValue)<>"constant" then
            error(msprintf("Wrong type for ''%s'' property: A %d-by-%d real vector expected.\n", "Version", 1, 3));
        end
        if or(size(propertyValue)<>[1 3]) then
            error(msprintf("Wrong size for ''%s'' property: A %d-by-%d real vector expected.\n", "Version", 1, 3));
        end
        projectObj(propertyName) = propertyValue;
    else
        error(msprintf("''%s'' is not a valid property name.\n", propertyName));
    end
endfunction

