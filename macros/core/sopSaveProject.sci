// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010 - DIGITEO - Vincent COUVERT
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopSaveProject(projPtr, projFileName)

    if argn(2)>1 then
        projPtr.pointedfile = projFileName;
        if isempty(projPtr.origfile) then
            projPtr.origfile = projFileName;
        end
    end

    if isempty(projPtr.pointedfile) then
        error(msprintf(gettext("''%s'' property is not set."), "PointedFile"));
    end

    // Update paths in models to be relative to projPtr.pointedfile
    // Filenames are stored as absoluted during the session to avoid problems with current path when simulating
    if projPtr.pointedfile==projPtr.origfile then // Final project file
        for kResp = 1:size(projPtr.responses)
            for kModel = 1:size(projPtr.responses(kResp).models)
                params = projPtr.responses(kResp).models(kModel).params;
                [ierr, wrapperParams] = sopCallWrapper(projPtr.responses(kResp).models(kModel).funname, "configure");
                for kParam=1:size(wrapperParams)
                    if wrapperParams(kParam).type=="Filename" then
                        fileParam = get_param(params, wrapperParams(kParam).name);
                        relativeFileParam = getrelativefilename(fileparts(projPtr.origfile, "path"), fileParam);
                        sopLog(fileParam + "==>" + relativeFileParam);
                        params = set_param(params, wrapperParams(kParam).name, relativeFileParam);
                        projPtr.responses(kResp).models(kModel).params = params;
                    end
                end
            end
        end
    end

    [status,message] = copyfile(projPtr.workfile, projPtr.pointedfile);

    if status == 0 then
        error(message);
    end

endfunction
