// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function statusObj = sopStatus(status)

    // Create the tlist
    statusObj = tlist(["sopstat", "value"])

    select convstr(status, "l")
    case "tobedone"
        statusObj.value = _("To be done");
    case "running"
        statusObj.value = _("Running");
    case "failed"
        statusObj.value = _("Failed");
    case "done"
        statusObj.value = _("Done");
    end
endfunction

