function Coords=som_unit_coords(msize,lattice,shape)
    //SOM_UNIT_COORDS Locations of units on the SOM grid.
    //
    // Co = som_unit_coords(topol, [lattice], [shape])
    //
    //  Co = som_unit_coords(sMap);
    //  Co = som_unit_coords(sMap.topol);
    //  Co = som_unit_coords(msize, 'hexa', 'cyl');
    //  Co = som_unit_coords([10 4 4], 'rect', 'toroid');
    //
    //  Input and output arguments ([]'s are optional):
    //   topol              topology of the SOM grid
    //             (struct) topology or map struct
    //             (vector) the 'msize' field of topology struct
    //   [lattice] (string) map lattice, 'rect' by default
    //   [shape]   (string) map shape, 'sheet' by default
    //
    //   Co        (matrix, size [munits k]) coordinates for each map unit
    //
    // For more help, try 'type som_unit_coords' or check out online documentation.
    // See also SOM_UNIT_DISTS, SOM_UNIT_NEIGHS.

    //%%%%%%%%%%%% DETAILED DESCRIPTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    //
    // som_unit_coords
    //
    // PURPOSE
    //
    // Returns map grid coordinates for the units of a Self-Organizing Map.
    //
    // SYNTAX
    //
    //  Co = som_unit_coords(sTopol);
    //  Co = som_unit_coords(sM.topol);
    //  Co = som_unit_coords(msize);
    //  Co = som_unit_coords(msize,'hexa');
    //  Co = som_unit_coords(msize,'rect','toroid');
    //
    // DESCRIPTION
    //
    // Calculates the map grid coordinates of the units of a SOM based on
    // the given topology. The coordinates are such that they can be used to
    // position map units in space. In case of 'sheet' shape they can be
    // (and are) used to measure interunit distances.
    //
    // NOTE: for 'hexa' lattice, the x-coordinates of every other row are shifted
    // by +0.5, and the y-coordinates are multiplied by sqrt(0.75). This is done
    // to make distances of a unit to all its six neighbors equal. It is not
    // possible to use 'hexa' lattice with higher than 2-dimensional map grids.
    //
    // 'cyl' and 'toroid' shapes: the coordinates are initially determined as
    // in case of 'sheet' shape, but are then bended around the x- or the
    // x- and then y-axes to get the desired shape.
    //
    // POSSIBLE BUGS
    //
    // I don't know if the bending operation works ok for high-dimensional
    // map grids. Anyway, if anyone wants to make a 4-dimensional
    // toroid map, (s)he deserves it.
    //
    // REQUIRED INPUT ARGUMENTS
    //
    //  topol          Map grid dimensions.
    //        (struct) topology struct or map struct, the topology
    //                 (msize, lattice, shape) of the map is taken from
    //                 the appropriate fields (see e.g. SOM_SET)
    //        (vector) the vector which gives the size of the map grid
    //                 (msize-field of the topology struct).
    //
    // OPTIONAL INPUT ARGUMENTS
    //
    //  lattice (string) The map lattice, either 'rect' or 'hexa'. Default
    //                   is 'rect'. 'hexa' can only be used with 1- or
    //                   2-dimensional map grids.
    //  shape   (string) The map shape, either 'sheet', 'cyl' or 'toroid'.
    //                   Default is 'sheet'.
    //
    // OUTPUT ARGUMENTS
    //
    //  Co   (matrix) coordinates for each map units, size is [munits k]
    //                where k is 2, or more if the map grid is higher
    //                dimensional or the shape is 'cyl' or 'toroid'
    //
    // EXAMPLES
    //
    // Simplest case:
    //  Co = som_unit_coords(sTopol);
    //  Co = som_unit_coords(sMap.topol);
    //  Co = som_unit_coords(msize);
    //  Co = som_unit_coords([10 10]);
    //
    // If topology is given as vector, lattice is 'rect' and shape is 'sheet'
    // by default. To change these, you can use the optional arguments:
    //  Co = som_unit_coords(msize, 'hexa', 'toroid');
    //
    // The coordinates can also be calculated for high-dimensional grids:
    //  Co = som_unit_coords([4 4 4 4 4 4]);
    //
    // SEE ALSO
    //
    //  som_unit_dists    Calculate interunit distance along the map grid.
    //  som_unit_neighs   Calculate neighborhoods of map units.

    // Copyright (c) 1997-2000 by the SOM toolbox programming team.
    // http://www.cis.hut.fi/projects/somtoolbox/

    // Version 1.0beta juuso 110997
    // Version 2.0beta juuso 101199 070600

    // Scilab version based on Matlab version and developed in the context of:
    // - OMD2 project (French National Agency for Research)
    // - CSDL project (Systematic competitiveness cluster)

    if(length(msize)==1)
        msize = [msize 1];
    end
    munits = prod(msize);
    mdim = length(msize);
    Coords = zeros(munits,mdim);

    k= [1 cumprod(msize(1:$-1))];
    inds = [0:(munits - 1)]';
    for i=mdim:-1:1
        Coords(:,i)=floor(inds/k(i));
        inds = rem(inds,k(i));
    end
    Coords = Coords(:,$:-1:1);

    if(lattice =='hexa')
        inds_row = (cumsum(ones(msize(2),1))-1)*msize(1);
        for i=2:2:msize(1)
            Coords(i+inds_row,1)=Coords(i+inds_row,1)+0.5;
        end
    end


    select shape
    case 'sheet'
        if(lattice=='hexa')
            Coords(:,2)=Coords(:,2)*sqrt(0.75);
        end
    end

endfunction
