function sMap=som_lininit(D,sMap)
    //SOM_LININIT Initialize a Self-Organizing Map linearly.
    //
    // sMap = som_lininit(D, [[argID,] value, ...])
    //
    //  sMap = som_lininit(D);
    //  sMap = som_lininit(D,sMap);
    //  sMap = som_lininit(D,'munits',100,'hexa');
    //
    //  Input and output arguments ([]'s are optional):
    //   D                 The training data.
    //            (struct) data struct
    //            (matrix) data matrix, size dlen x dim
    //   [argID,  (string) Parameters affecting the map topology are given
    //    value]  (varies) as argument ID - argument value pairs, listed below.
    //   sMap     (struct) map struct
    //
    // Here are the valid argument IDs and corresponding values. The values
    // which are unambiguous (marked with '*') can be given without the
    // preceeding argID.
    //  'munits'       (scalar) number of map units
    //  'msize'        (vector) map size
    //  'lattice'     *(string) map lattice: 'hexa' or 'rect'
    //  'shape'       *(string) map shape: 'sheet', 'cyl' or 'toroid'
    //  'topol'       *(struct) topology struct
    //  'som_topol','sTopol'    = 'topol'
    //  'map'         *(struct) map struct
    //  'som_map','sMap'        = 'map'
    //
    // For more help, try 'type som_lininit' or check out online documentation.
    // See also SOM_MAP_STRUCT, SOM_RANDINIT, SOM_MAKE.

    //%%%%%%%%%%%% DETAILED DESCRIPTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    //
    // som_lininit
    //
    // PURPOSE
    //
    // Initializes a SOM linearly along its greatest eigenvectors.
    //
    // SYNTAX
    //
    //  sMap = som_lininit(D);
    //  sMap = som_lininit(D,sMap);
    //  sMap = som_lininit(D,'munits',100,'hexa');
    //
    // DESCRIPTION
    //
    // Initializes a SOM linearly. If necessary, a map struct is created
    // first. The initialization is made by first calculating the eigenvalues
    // and eigenvectors of the training data. Then, the map is initialized
    // along the mdim greatest eigenvectors of the training data, where
    // mdim is the dimension of the map grid.
    //
    // REFERENCES
    //
    // Kohonen, T., "Self-Organizing Map", 2nd ed., Springer-Verlag,
    //    Berlin, 1995, pp. 106-107.
    //
    // REQUIRED INPUT ARGUMENTS
    //
    //  D                 The training data.
    //           (struct) Data struct. If this is given, its '.comp_names' and
    //                    '.comp_norm' fields are copied to the map struct.
    //           (matrix) data matrix, size dlen x dim
    //
    // OPTIONAL INPUT ARGUMENTS
    //
    //  argID (string) Argument identifier string (see below).
    //  value (varies) Value for the argument (see below).
    //
    //  The optional arguments can be given as 'argID',value -pairs. If an
    //  argument is given value multiple times, the last one is used.
    //
    //  Here are the valid argument IDs and corresponding values. The values
    //  which are unambiguous (marked with '*') can be given without the
    //  preceeding argID.
    //  'dlen'         (scalar) length of the training data
    //  'data'         (matrix) the training data
    //                *(struct) the training data
    //  'munits'       (scalar) number of map units
    //  'msize'        (vector) map size
    //  'lattice'     *(string) map lattice: 'hexa' or 'rect'
    //  'shape'       *(string) map shape: 'sheet', 'cyl' or 'toroid'
    //  'topol'       *(struct) topology struct
    //  'som_topol','sTopol'    = 'topol'
    //  'map'         *(struct) map struct
    //  'som_map','sMap'        = 'map'
    //
    // OUTPUT ARGUMENTS
    //
    //  sMap     (struct) The initialized map struct.
    //
    // EXAMPLES
    //
    //  sMap = som_lininit(D);
    //  sMap = som_lininit(D,sMap);
    //  sMap = som_lininit(D,'msize',[10 10]);
    //  sMap = som_lininit(D,'munits',100,'rect');
    //
    // SEE ALSO
    //
    //  som_map_struct   Create a map struct.
    //  som_randinit     Initialize a map with random values.
    //  som_make         Initialize and train self-organizing map.

    // Copyright (c) 1997-2000 by the SOM toolbox programming team.
    // http://www.cis.hut.fi/projects/somtoolbox/

    // Version 1.0beta ecco 100997
    // Version 2.0beta 101199

    // Scilab version based on Matlab version and developed in the context of:
    // - OMD2 project (French National Agency for Research)
    // - CSDL project (Systematic competitiveness cluster)

    [dlen dim]=size(D);
    [munits dim2]=size(sMap.codebook);
    if(dim2 ~= dim)
        error('Map and data must have the same dimension')
    end
    msize = sMap.topol.msize;
    mdim = length(msize);
    munits = prod(msize);

    A=zeros(dim,dim);
    me = zeros(1,dim);
    for i=1:dim
        me(i)=mean(D(abs(D(:,i))<%inf,i),'r');
        D(:,i)=D(:,i)-me(i);
    end
    for i=1:dim
        for j=i:dim
            c=D(:,i).*D(:,j);
            c=c(abs(c)<%inf);
            A(i,j)=sum(c)/length(c);
            A(j,i)=A(i,j);
        end
    end
    [V,S]=spec(A);
    eigval = diag(S);
    [y,ind]=gsort(eigval,'r','i');
    eigval=eigval(ind($:-1:1));
    V=V(:,ind($:-1:1));
    V=V(:,1:mdim);
    eigval = eigval(1:mdim);

    for i=1:mdim
        V(:,i) = (V(:,i)/norm(V(:,i)))*sqrt(eigval(i));
    end

    //    me=zeros(1,dim);
    //    V=zeros(1,dim);
    //    for i=1:dim
    //        inds = find(~isnan(D(:,i)));
    //        me(i)=mean(D(inds,i),'r')
    //        V(i)=sqrt(variance(D(inds,i),'r'));
    //    end

    sMap.codebook = me(ones(munits,1),:);

    Coords = som_unit_coords(msize,'rect','sheet')


    cox = Coords(:,1);
    Coords(:,1) = Coords(:,2);
    Coords(:,2) = cox;

    for i=1:mdim
        ma = max(Coords(:,i));
        mi = min(Coords(:,i));
        if(ma > mi)
            Coords(:,i)=(Coords(:,i)-mi)/(ma-mi);
        else
            Coords(:,i) = 0.5;
        end
    end
    Coords = (Coords - 0.5)*2;

    for n=1:munits
        for d=1:mdim
            sMap.codebook(n,:)=sMap.codebook(n,:)+Coords(n,d)*V(:,d)';
        end
    end
    som_train = struct('type','som_train',...
    'algorithm','lininit',...
    'data_name','',....
    'mask',[],...
    'neigh','gaussian',...
    'radius_ini',%nan,...
    'radius_fin',%nan,...
    'alpha_ini',%nan,...
    'alpha_type','inv',...
    'trainlen',%nan,...
    'time','')


endfunction
