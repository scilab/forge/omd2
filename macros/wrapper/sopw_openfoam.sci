// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function value = sopw_openfoam(action, object)

    value = [];

    select action
    case "type"
        value = "simulator";

    case "description"
        value = "OpenFOAM simulator";

    case "version"
        value = [0 0 0];

    case "atomsmodules"

    case "functionsneeded"

    case "defaults"
        value = init_param();
        value = add_param(value, "pathToBashrc", "/store/softs/OpenFOAM/OpenFOAM-1.7.1/etc/bashrc");
        value = add_param(value, "openFOAMTempatePath", sopModulePath() + "examples/testcase1/OpenFoam/Default/");
        value = add_param(value, "controlDictFileFun", "make_controlDict_file");
        value = add_param(value, "numberOfIterations", 1000);
        value = add_param(value, "blockMeshDictFileFun", "make_mesh_file");
        value = add_param(value, "density", 40);
        value = add_param(value, "postprocessingFun", "testcase1PostProcessing");

    case "configure"
        pathToBashrc = sopParameter("pathToBashrc", "String", ..
                                    "/store/softs/OpenFOAM/OpenFOAM-1.7.1/etc/bashrc", ..
                                    "/store/softs/OpenFOAM/OpenFOAM-1.7.1/etc/bashrc");
        openFOAMTempatePath = sopParameter("openFOAMTempatePath", "String", ..
                                           sopModulePath() + "examples/testcase1/OpenFoam/Default/", ..
                                           sopModulePath() + "examples/testcase1/OpenFoam/Default/"); // TODO remove hard coded path
        controlDictFileFun = sopParameter("controlDictFileFun", "FunctionName", "make_controlDict_file");
        numberOfIterations = sopParameter("numberOfIterations", "Constant", [0 %inf], 1000);
        blockMeshDictFileFun = sopParameter("blockMeshDictFileFun", "FunctionName", "make_mesh_file");
        density = sopParameter("density", "Constant", [0 %inf], 40);
        postprocessingFun = sopParameter("postprocessingFun", "FunctionName", "testcase1PostProcessing");
        value = list(pathToBashrc, openFOAMTempatePath, controlDictFileFun, numberOfIterations, blockMeshDictFileFun, density, postprocessingFun);
    case "simulate"
        pointsToSimulate = object.points
        pathToBashrc = object.pathToBashrc;
        openFOAMTempatePath = object.openFOAMTempatePath;
        controlDictFileFun = object.controlDictFileFun;
        numberOfIterations = object.numberOfIterations;
        blockMeshDictFileFun = object.blockMeshDictFileFun;
        density = object.density;
        postprocessingFun = object.postprocessingFun;
        numberOfResponses = size(object.responses);

        if %T then
            InputFiles = list();
            files = ls(openFOAMTempatePath);
            for k = 1:size(files, "*")
                if isdir(openFOAMTempatePath + filesep() + files(k)) then
                    subfiles = ls(openFOAMTempatePath + filesep() + files(k));
                    for sk = 1:size(subfiles, "*")
                        if isdir(openFOAMTempatePath + filesep() + files(k) + filesep() + subfiles(sk)) then
                            subsubfiles = ls(openFOAMTempatePath + filesep() + files(k) + filesep() + subfiles(sk));
                            for ssk = 1:size(subsubfiles, "*")
                                InputFiles($+1) = getrelativefilename(pwd(), openFOAMTempatePath + filesep() + files(k) + filesep() + subfiles(sk)+ filesep() + subsubfiles(ssk));
                            end
                            
                        else
                            InputFiles($+1) = getrelativefilename(pwd(), openFOAMTempatePath + filesep() + files(k) + filesep() + subfiles(sk));
                        end
                    end
                else
                    InputFiles($+1) = getrelativefilename(pwd(),openFOAMTempatePath + filesep() + files(k));
                end
            end
            //PAconnect("rmi://giraglia.inria.fr:1099/");
            if isdir(pwd() + filesep() + ".PAScheduler") then
                disp("Remove .PAScheduler directory")
                rmdir(pwd() + filesep() + ".PAScheduler")
            end
            PAconnect("pamr://1");
            PAoptions("Debug",%F);
            t = PATask(1, size(pointsToSimulate, 1));
            for k=1:size(pointsToSimulate, 1)
                t(1,k).Func = "openfoamSimulator";
                t(1,k).Params = list(pointsToSimulate(k,:), ..
                                     pathToBashrc, ..
                                     getrelativefilename(pwd(),openFOAMTempatePath), ..
                                     controlDictFileFun, ..
                                     numberOfIterations, ..
                                     blockMeshDictFileFun, ..
                                     density, ..
                                     postprocessingFun, ..
                                     numberOfResponses)
                t(1,k).Sources = list(sopModulePath() + "macros/core/openfoamSimulator.sci", ..
                                      sopModulePath() + "macros/core/sopLog.sci", ..
                                      sopModulePath() + "macros/core/sopWarning.sci", ..
                                      sopModulePath() + "macros/core/sopError.sci", ..
                                      sopModulePath() + "examples/testcase1/make_controlDict_file.sci", ..
                                      sopModulePath() + "examples/testcase1/make_mesh_file.sci", ..
                                      sopModulePath() + "examples/testcase1/testcase1PostProcessing.sci", ..
                                      sopModulePath() + "examples/testcase1/make_profile.sci", ..
                                      sopModulePath() + "examples/testcase1/make_index.sci", ..
                                      sopModulePath() + "examples/testcase1/get_U_and_p.sci", ..
                                      sopModulePath() + "examples/testcase1/compute_criteria.sci");
                t(1,k).InputFiles = InputFiles;
                t(1,k).SelectionScript = "http://proactive.inria.fr/pacagrid/selection/computingNodes.js";
            end
            r = PAsolve(t);
            values  = PAwaitFor(r)
            simulatedResponses = matrix(list2vec(values), numberOfResponses, -1)';
            object.values = simulatedResponses;
        else
            pathToBashrc = "/opt/openfoam171/etc/bashrc"
            simulatedResponses = openfoamSimulator(pointsToSimulate, ..
                                                   pathToBashrc,..
                                                   openFOAMTempatePath, ..
                                                   controlDictFileFun, ..
                                                   numberOfIterations, ..
                                                   blockMeshDictFileFun, ..
                                                   density, ..
                                                   postprocessingFun, ..
                                                   numberOfResponses);

            // plot : 
            // ContourDiscretise = make_profile(20, factorsValues)
            // plot(ContourDiscretise(1,:),ContourDiscretise(2,:),'r'); // Haut du conduit
            // plot(ContourDiscretise(3,:),ContourDiscretise(4,:),'b'); // Bas du conduit
            object.values = simulatedResponses;
        end

    else
        error(msprintf(_("%s: Unknown action ''%s''.\n"), "sopw_openfoam", action))
    end

endfunction