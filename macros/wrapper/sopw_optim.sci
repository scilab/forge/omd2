// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function value = sopw_optim(action, object)
    
    value = [];
    
    select action
    case "type"
        value = "optimizer";

    case "description"
        value = "Optim function wrapper";

    case "atomsmodules"

    case "functionsneeded"
    
    case "defaults"
        value = init_param();

    case "configure"
        value = list();
        
    case "version"
        value = [0 0 0];
        
    case "optimize"
        costf = evstr("sopObjectiveFunction");
        lb = [];
        ub = [];
        for kFact = 1:size(object.factors)
            if object.factors(kFact).isfixed then
                lb = [lb object.factors(kFact).nominalvalue];
                ub = [ub object.factors(kFact).nominalvalue];
            else
                lb = [lb object.factors(kFact).minbound];
                ub = [ub object.factors(kFact).maxbound];
            end
        end
        if isempty(object.optimalpoint) then // First optimizer called
            x0 = [];
            for kFact = 1:size(object.factors)
                x0 = [x0 object.factors(kFact).nominalvalue];
            end
        else
            x0 = object.optimalpoint; // Second optimizer called
        end
        
        [fopt, xopt] = optim(list(costf, object), "b", lb, ub, x0)
        object.objfunctionoptimalvalue = fopt;
        object.optimalpoint = xopt;

    else
        error(msprintf(_("%s: Unknown action ''%s''.\n"), "sopw_optim", action))
    end
    
endfunction

