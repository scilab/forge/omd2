// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function value = sopw_proactive(action, object)
    
    value = [];
    
    select action
    case "type"
        value = "";
        
    case "description"
        value = "ProActive wrapper";
        
    case "version"
        value = [0 0 0];

    case "enabled"
        value = exists("toolbox_proactivelib")==1;
    
    case "atomsmodules"
        
    case "functionsneeded"
    
    case "defaults"
        value = init_param();
        value = add_param(value, "url", "pamr://1");
        value = add_param(value, "debug", %F);
   
    case "configure"
        url = sopParameter("url", "String", [], "pamr://1");
        _debug = sopParameter("debug", "Boolean", [%T %F], %F);
        value = list(url, _debug)
        
    else
        error(msprintf(_("%s: Unknown action ''%s''.\n"), "sopw_proactive", action))
    end
    
endfunction

