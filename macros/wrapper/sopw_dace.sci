// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function value = sopw_dace(action, object)
    
    value = [];
    
    select action
    case "type"
        value = "modeler";
        
    case "description"
        value = "DACE wrapper";
        
    case "version"
        value = [0 0 0];

    case "atomsmodules"
        
    case "functionsneeded"
    
    case "defaults"
        value = init_param();
        value = add_param(value, "regr", "regpoly1");
        value = add_param(value, "_corr", "corrgauss");
        value = add_param(value, "theta0", 0.05);
        value = add_param(value, "lob", 0.01);
        value = add_param(value, "upb", 0.9);
   
    case "configure"
        regr = sopParameter("regr", "Function", list("regpoly0", "regpoly1", "regpoly2"), "regpoly1");
        _corr = sopParameter("_corr", "Function", list("corrcubic", "correxp", "correxpg", "corrgauss", "corrlin", "corrspherical", "corrspline"), "corrgauss");
        theta0 = sopParameter("theta0", "Constant", [-%inf +%inf], 0.05);
        lob = sopParameter("lob", "Constant", [-%inf +%inf], 0.01);
        upb = sopParameter("upb", "Constant", [-%inf +%inf], 0.9);
        value = list(regr, _corr, theta0, lob, upb)
    case "model"
        S = object.learningpoints;
        Y = object.learningresponses;
        modelingParams = object.params;
        regr = get_param(modelingParams, "regr");
        _corr = get_param(modelingParams, "_corr")
        theta0 = get_param(modelingParams, "theta0")
        lob = get_param(modelingParams, "lob")
        upb = get_param(modelingParams, "upb")
        [dmodel, perf] = dacefit(S, Y, evstr(regr), evstr(_corr), theta0, lob, upb);
        object.coeffs = dmodel;
        
    case "estimate"
        x = object.points
        dmodel = object.coeffs;
        [predictedResponses, err] = predictor(x, dmodel)
        object.predictions = predictedResponses;

    else
        error(msprintf(_("%s: Unknown action ''%s''.\n"), "sopw_dace", action))
    end
    
endfunction

