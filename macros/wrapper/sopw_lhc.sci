// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function value = sopw_lhc(action, object)

    value = [];

    select action
    case "type"
        value = "doegenerator";

    case "description"
        value = "Latin Hypercube Sampling";

    case "version"
        value = [0 0 0];

    case "atomsmodules"

    case "functionsneeded"

    case "defaults"
        value = init_param();
        value = add_param(value, "m", 30);

    case "configure"
        m = sopParameter("m", "Constant", [0 %inf], 30);
        value = list(m);
        
    case "generate"
        m = object.m;
        n = object.doevariablesnumber;
        generatedDoe = lhcgenerator(m, n);
        object.values = generatedDoe;
        
    else
        error(msprintf(_("%s: Unknown action ''%s''.\n"), "sopw_lhc", action))
    end

endfunction