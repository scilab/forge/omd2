// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012 - Scilab Enterprises - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopLogView(event, objectHandle, varargin)
    
    select event
    case "objectcreated"
        mprintf("Object with identifier %d has been created.\n", objectHandle.identifier)

    case "objectupdated"
        mprintf("Object with identifier %d has been updated.\n", objectHandle.identifier)
        mprintf("Updated property: %s.\n", varargin(1))
        mprintf("New value: %s.\n", string(varargin(2)))

    case "objectdeleted"
        mprintf("Object with identifier %d has been deleted.\n", objectHandle.identifier)

    else
        error(msprintf(gettext("%s: Unhandled event: ''%s''.\n"), "sopLogView", event))
    end

endfunction
