// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012 - Scilab Enterprises - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function varargout = sopDataController(action, varargin)

    varargout = list()

    sopDataControllerFile = fullfile(TMPDIR, "sopdatacontroller.sod");
    
    global sopdatacontroller
    if isempty(sopdatacontroller) then
        if ~isfile(sopDataControllerFile) then
            // Controller has not been initialized yet
            sopdatacontroller = struct();
            sopdatacontroller.views = list("sopLogView");
            save(sopDataControllerFile, "sopdatacontroller")
        else
           // Controller has been cleared by clearglobal
            load(sopDataControllerFile)
        end
    end
    
    select action
    case "askobject"
        objectType = varargin(1);
        // Call model to create the object
        objectHandle = sopDataModel("askobject", objectType)
        
        // Inform all views that an object has been created
        views = sopdatacontroller.views;
        for kView = 1:size(views)
            if typeof(views(kView)) == "handle" then // A figure
            elseif typeof(views(kView)) == "string" then // Log view
                func = views(kView);
                execstr(func + "(""objectcreated"", objectHandle)");
            else
                error(msprintf(gettext("%s: Unhandled view type: ''%s''.\n"), "sopDataController", typeof(views(kView))));
            end
        end
        varargout(1) = objectHandle
    case "setproperty"
        objectHandle = varargin(1);
        propertyName = varargin(2);
        propertyValue = varargin(3);
        // Call model to update the object
        varargout(1) = sopDataModel("setproperty", objectHandle, propertyName, propertyValue);
        
        // Inform all views that an object has been updated
        views = sopdatacontroller.views;
        for kView = 1:size(views)
            if typeof(views(kView)) == "handle" then // A figure
            elseif typeof(views(kView)) == "function" then // Log view
                func = views(kView);
                func("objectupdated", objectHandle, propertyName, propertyValue);
            else
                error(msprintf(gettext("%s: Unhandled view type: ''%s''.\n"), "sopDataController", typeof(views(kView))));
            end
        end
    case "getproperty"
        objectHandle = varargin(1);
        propertyName = varargin(2);
        // Call model to update the object
        varargout(1) = sopDataModel("getproperty", objectHandle, propertyName)
        
        // Inform all views that an object has been updated
        views = sopdatacontroller.views;
        for kView = 1:size(views)
            if typeof(views(kView)) == "handle" then // A figure
            elseif typeof(views(kView)) == "function" then // Log view
                func = views(kView);
                func("objectupdated", objectHandle, propertyName, propertyValue);
            else
                error(msprintf(gettext("%s: Unhandled view type: ''%s''.\n"), "sopDataController", typeof(views(kView))));
            end
        end
    else
        error(msprintf(gettext("%s: Unhandled action: ''%s''.\n"), "sopDataModel", action))
    end
endfunction
