// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012 - Scilab Enterprises - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function varargout = sopDataModel(action, varargin)
    //sopSetObjectProperty(object, "propertyname", propertyvalue)
    //propertyValue = sopGetObjectProperty("get", object, "propertyname")
    //sopDeleteObject(object);

   varargout = list();

    select action
    case "askobject"
        objectType = varargin(1);
        varargout(1) = sopDataModelCreate(objectType)
    case "setproperty"
        objectHandle = varargin(1);
        propertyName = varargin(2);
        propertyValue = varargin(3);
        varargout(1) = sopDataModelSetProperty(objectHandle, propertyName, propertyValue)
    case "getproperty"
        objectHandle = varargin(1);
        propertyName = varargin(2);
        varargout(1) = sopDataModelGetProperty(objectHandle, propertyName)
    else
        error(msprintf(gettext("%s: Unhandled action: ''%s''.\n"), "sopDataModel", action))
    end

endfunction

function objectHandle = sopDataModelCreate(objectType)

    sopDataModelFile = fullfile(TMPDIR, "sopdatamodel.sod");

    global sopdatamodel
    if isempty(sopdatamodel) then
        if ~isfile(sopDataModelFile) then
            // Controller has not been initialized yet
            sopdatamodel = list();
            save(sopDataModelFile, "sopdatamodel")
        else
            // Controller has been cleared by clearglobal
            load(sopDataModelFile)
        end
    end

    select objectType
    //*********//
    // PROJECT //
    //*********//
    case "project"
        // Create the tlist
        projectObj = tlist(["sopproj", "<version>", "<description>", "<filedata>", "<variables>", "<factors>", "<responses>", "<doenumerotation>", "<doecenterpointindex>", ..
        "<datasource>", "<doegenerators>", ..
        "<optimizers>", "<responsescoeffs>", ..
        "<optimalpoint>", "<objfunctionoptimalvalue>", ..
        "<dataconfigstatus>", "<modelconfigstatus>", "<optimconfigstatus>", ..
        "<dataexecstatus>", "<modelexecstatus>", "<optimexecstatus>", ..
        "<simulator>", ..
        "<paparams>"])

        // Set default values
        projectObj.version = sopVersion();
        projectObj.description = "";
        projectObj.filedata = list(); // A filedata is a 'variable' tlist (save data)
        projectObj.variables = list(); // A variable is a 'variable' tlist
        projectObj.factors = list(); // A parameter is a 'variable' tlist
        projectObj.responses = list(); // A response is a 'variable' tlist
        projectObj.doenumerotation = []; // A 'variable' tlist
        projectObj.doecenterpointindex = 0;
        projectObj.datasource = "file"; // Can be "file" or "doe"
        projectObj.doegenerators = list();
        projectObj.optimizers = list();
        projectObj.responsescoeffs = [];
        projectObj.optimalpoint = [];
        projectObj.objfunctionoptimalvalue = [];
        projectObj.dataconfigstatus = sopStatus("Done"); // Data to be defined // TODO
        projectObj.modelconfigstatus = sopStatus("Done"); // Modelization to be defined // TODO
        projectObj.optimconfigstatus = sopStatus("Done"); // Optimization to be defined // TODO
        projectObj.dataexecstatus = sopStatus("ToBeDone"); // Data to be defined
        projectObj.modelexecstatus = sopStatus("ToBeDone"); // Modelization to be defined
        projectObj.optimexecstatus = sopStatus("ToBeDone"); // Optimization to be defined
        projectObj("<simulator>") = []; // To avoid the test, require a 'sopsimul' tlist
        [ierr, paparams] = sopCallWrapper("sopw_proactive", "defaults");
        if ierr<>0 then
            error()
        end
        projectObj.paparams = paparams;
        sopdatamodel($+1) = projectObj;
    case "variable"
    case "factor"
    case "response"
    else
        error(msprintf(_("%s: Wrong value for input argument #%d: ''%s'', ''%s'', ''%s'' or ''%s'' expected.\n"), "sopDataModel", 1, "project", "variable", "factor", "response"));
    end
    objectHandle = tlist(["sophandle", "identifier"], size(sopdatamodel));
endfunction

function status = sopDataModelSetProperty(objectHandle, propertyName, propertyValue)
    
    status = %T

    sopDataModelFile = fullfile(TMPDIR, "sopdatamodel.sod");

    global sopdatamodel
    if isempty(sopdatamodel) then
        if ~isfile(sopDataModelFile) then
            // Controller has not been initialized yet
            sopdatamodel = list();
            save(sopDataModelFile, "sopdatamodel")
        else
            // Controller has been cleared by clearglobal
            load(sopDataModelFile)
        end
    end

    updatedObject = sopdatamodel(objectHandle.identifier);
    updatedObject("<" + propertyName + ">") = propertyValue;
    sopdatamodel(objectHandle.identifier) = updatedObject;
    
    save(sopDataModelFile, "sopdatamodel");
endfunction

function propertyValue = sopDataModelGetProperty(objectHandle, propertyName)

    sopDataModelFile = fullfile(TMPDIR, "sopdatamodel.sod");

    global sopdatamodel
    if isempty(sopdatamodel) then
        if ~isfile(sopDataModelFile) then
            // Controller has not been initialized yet
            sopdatamodel = list();
            save(sopDataModelFile, "sopdatamodel")
        else
            // Controller has been cleared by clearglobal
            load(sopDataModelFile)
        end
    end

    updatedObject = sopdatamodel(objectHandle.identifier);
    propertyValue = updatedObject("<" + propertyName + ">");
endfunction
