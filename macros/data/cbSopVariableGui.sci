// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function cbSopVariableGui()

    sopvariable = findobj("Tag", "sopvariablegui");

    sopvariablegui = findobj("Tag", "sopvariablegui");
    editedVariable# = sopvariablegui.userdata.sopeditedvariable
    
    gcboTag = get(gcbo, "Tag");
    
    parameters = ["Fixed" "Max value", "Nominal value" "Min value" "Name"];
    fields = ["isfixed" "maxbound" "nominalvalue" "minbound" "name"];
    nbParams = size(parameters, "*")
    
    isFixed = get(findobj("Tag", "isfixed"), "Value") == get(findobj("Tag", "isfixed"), "Max");
    if isFixed then
        set(findobj("Tag", "minbound"), "Enable", "off");
        set(findobj("Tag", "maxbound"), "Enable", "off");
    else
        set(findobj("Tag", "minbound"), "Enable", "on");
        set(findobj("Tag", "maxbound"), "Enable", "on");
    end

    // Manage OK button status
    // Will not be enabled if data are not valid
    okEnable = "on";
    variableName = get(findobj("Tag", "name"), "String");
    [variableMinBound, errStatus] = evstr(get(findobj("Tag", "minbound"), "String"));
    if errStatus<>0 then
        okEnable = "off";
    end
    [variableNominalValue, errStatus] = evstr(get(findobj("Tag", "nominalvalue"), "String"));
    if errStatus<>0 then
        okEnable = "off";
    end
    [variableMaxBound, errStatus] = evstr(get(findobj("Tag", "maxbound"), "String"));
    if errStatus<>0 then
        okEnable = "off";
    end

    if okEnable == "on" then // All values could be read
        if variableMinBound > variableMaxBound then
            okEnable = "off";
        end
        if variableNominalValue > variableMaxBound then
            okEnable = "off";
        end
        if variableNominalValue < variableMinBound then
            okEnable = "off";
        end
    end
    
    // TODO check that name does not already exist

    set(findobj("Tag", "ok_btn"), "Enable", okEnable);

    select gcboTag
    case "cancel_btn"
        delete(sopvariablegui);
        h = findobj("tag", "sopdatagui");
        sopSetGuiEnable(h, "on");
    case "ok_btn"
        if okEnable=="off" then // Focus lost by editbox when OK button was clicked
            return
        end
    
        editedVariable#.name = variableName;
        editedVariable#.minbound = variableMinBound;
        editedVariable#.maxbound = variableMaxBound;
        editedVariable#.nominalvalue = variableNominalValue;
        editedVariable#.isfixed = isFixed;
        
        delete(sopvariablegui);
        h = findobj("tag", "sopdatagui");
        sopSetGuiEnable(h, "on");
    end
endfunction
