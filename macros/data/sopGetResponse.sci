// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function varObj = sopGetResponse(sopProj, varIndexOrName)

    if size(varIndexOrName, "*")<>1 then
        error(msprintf(_("%s: Wrong type for input argument #%d: A real scalar or a string expected.\n"), "sopGetResponse", 2))
    end

    if typeof(varIndexOrName) == "constant" then
        varIndex = varIndexOrName;
    elseif typeof(varIndexOrName) == "string" then
        varIndex = [];
        for kVar = 1:sopGetNbResponses(sopProj)
            if or(sopProj.responses(kVar).name==varIndexOrName) then
                varIndex = kVar;
                break
            end
        end
    else
        error(msprintf(_("%s: Wrong type for input argument #%d: A real scalar/vector, a string or a string vector exepected.\n"), "sopAddResponse", 2))
    end
    
    varObj = sopProj.responses#(varIndex);
endfunction

