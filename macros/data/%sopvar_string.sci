// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function t = %sopvar_string(variableObj)
    t      = "Scilab Optimisation Platform variable";
    t($+1) = "=====================================";
    t($+1) = "Name: " + variableObj.name;
    t($+1) = "Units: " + string(variableObj.units);
    t($+1) = "IndexInFile: " + string(variableObj.indexinfile);
    t($+1) = "Values: [" + string(size(variableObj.values, "*")) + " values of type " + typeof(variableObj.values) + "]";
    if size(variableObj.valuestype, "*")==0 then
        t($+1) = "ValuesType: []";
    else
        t($+1) = "ValuesType: [" + string(size(variableObj.valuestype, "*")) + " values of type " + typeof(variableObj.valuestype) + "]";
    end
    t($+1) = "Models: [" + string(size(variableObj.models)) + " models]"
endfunction
