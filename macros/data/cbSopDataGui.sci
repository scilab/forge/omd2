// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function cbSopDataGui(gcboTag)

    if argn(2) < 1 then
        gcboTag = get(gcbo, "Tag");
    end

    h = findobj("Tag", "sopdatagui");

    select gcboTag
    case "addfactor_btn"
        sopVariableGui(%T, gcboTag)
        return // Avoid the execution of updateSopDataGui();
    case "addgenerator_btn"
        allGenerators = sopGetWrapper("DoeGenerator");
        allGeneratorsDesc = [];
        for kGenerator = 1:size(allGenerators)
            allGeneratorsDesc($+1) = allGenerators(kGenerator).description;
        end
        selectedItem = x_choose(allGeneratorsDesc, _("Select a generator"), _("Cancel"));
        if selectedItem==0 then // The user cancels
            return
        end

        sopProj = sopGetCurrentProject(h);

        sopProj.doegeneratorname($+1) = sopInitWrapper(allGenerators(selectedItem));
    case "addresponse_btn"
        sopVariableGui(%F, gcboTag)
        return // Avoid the execution of updateSopDataGui();
    case "close_btn"
        btn = messagebox(_("Do you want to quit Data module?"), _("Quit"), "question", ["Yes" "No"], "modal");
        if btn==1 then
            sopProj = sopGetCurrentProject(h);
            delete(h);
            mdelete(sopProj.workfile);
            sopSetGuiEnable(findobj("tag", "sopgui"), "on");
        end
        return // Avoid the execution of updateSopDataGui();
    case "configuresimulator_btn"
        sopProj = sopGetCurrentProject(h);

        funname = sopProj.simulator.funname;
        [ierr, inparams] = sopCallWrapper(funname, "configure");
        if ierr<>0 then
            error(msprintf(_("%s: Could not get configuration for ''%s'' wrapper.\n"), "cbSopDataGui", funname));
        end
        sopParametersConfigGui(h, "configuresimulator_btn", inparams, sopProj.simulator);

        return // Avoid the execution of updateSopDataGui();
    case "delfactor_btn"
        sopProj = sopGetCurrentProject(h);
        factorsListbox = findobj("Tag", "factors_listbox");
        selectedFactor = factorsListbox.value;
        sopProj.factors(selectedFactor) = null();
        sopSetCurrentProject(h, sopProj);
    case "delgenerator_btn"
        sopProj = sopGetCurrentProject(h);

        generatorsListBox = findobj("Tag", "generators_listbox");
        selectedGenerator = generatorsListBox.value;

        allGenerators = sopProj.doegeneratorname;
        allGenerators(selectedGenerator) = [];
        sopProj.doegeneratorname = allGenerators;

        allGeneratorsInParams = sopProj.doegeneratorinparams;
        allGeneratorsInParams(selectedGenerator) = null();
        sopProj.doegeneratorinparams = allGeneratorsInParams;

        allGeneratorsOutParams = sopProj.doegeneratoroutparams;
        allGeneratorsOutParams(selectedGenerator) = null();
        sopProj.doegeneratoroutparams = allGeneratorsOutParams;

        sopSetCurrentProject(h, sopProj);
    case "delresponse_btn"
        sopProj = sopGetCurrentProject(h);
        responsesListbox = findobj("Tag", "responses_listbox");
        selectedResponse = responsesListbox.value;
        sopProj.responses(selectedResponse) = null();
        sopSetCurrentProject(h, sopProj);
    case "editfactor_btn"
        sopProj = sopGetCurrentProject(h);
        factorsListbox = findobj("Tag", "factors_listbox");
        selectedFactor = factorsListbox.value;
        sopVariableGui(%T, gcboTag, sopProj.factors#(selectedFactor))
        return // Avoid the execution of updateSopDataGui();
    case "editgenerator_btn"
        sopProj = sopGetCurrentProject(h);

        generatorsListBox = findobj("Tag", "generators_listbox");
        selectedGenerator = generatorsListBox.value;

        funname = sopProj.doegenerators(selectedGenerator).funname;
        [ierr, params] = sopCallWrapper(funname, "configure");
        if ierr<>0 then
            error(msprintf(_("%s: Could not get configuration for ''%s'' wrapper.\n"), "cbSopDataGui", funname));
        end

        sopParametersConfigGui(h, "editgenerator_btn", params, sopProj.doegenerators#(selectedGenerator));
        return // Avoid the execution of updateSopDataGui();
    case "editresponse_btn"
        sopProj = sopGetCurrentProject(h);
        responsesListbox = findobj("Tag", "responses_listbox");
        selectedResponse = responsesListbox.value;
        sopVariableGui(%F, gcboTag, sopProj.responses#(selectedResponse))
        return // Avoid the execution of updateSopDataGui();
    case "factors_listbox"
        // Nothing to do
    case "factorstovariables_btn"
        sopProj = sopGetCurrentProject(h);
        factorsListbox = findobj("Tag", "factors_listbox");
        selectedFactors = factorsListbox.value;
        sopProj.variablesfromfactors = selectedFactors;
        sopSetCurrentProject(h, sopProj);
    case "generators_listbox"
        // Nothing to do
    case "generatefactors_menu"
        sopProj = sopGetCurrentProject(h);
        sopDoeGeneration(sopProj);
    case "loaddatafile_menu"
        [filename, pathname] = uigetfile(["*.db"; "*.txt"], pwd(), _("Select a file"), %T);
        if ~isempty(filename) & ~isempty(pathname) then
            for kFile=1:size(filename, "*")
                curProj = sopGetCurrentProject(h);
                if sopGetNbVariables(curProj)==0 & sopGetNbResponses(curProj)==0 & sopGetNbFactors(curProj)==0 & size(filename, "*")==1 then // Empty project
                    validationPoints = %F;
                else
                    // Use data as validation point ? Cancel ?
                    btn = messagebox("Load ''" + filename(kFile) + "'' as validation points", "Points type", "question", ["Yes", "No", "Cancel"], "modal");
                    select btn
                    case 0
                        // Window closed
                        return
                    case 1
                        validationPoints = %T;
                    case 2
                        validationPoints = %F;
                    case 4
                        // Cancel button
                        return
                    end
                end

                sopLoadDataFile(curProj, pathname + filesep() + filename(kFile), validationPoints);
                sopSetCurrentProject(h, curProj);
            end
        end
    case "quit_menu"
        btn = messagebox(_("Do you want to quit Data module?"), _("Quit"), "question", ["Yes" "No"], "modal");
        if btn==1 then
            sopProj = sopGetCurrentProject(h);
            mdelete(sopProj.workfile);
            delete(findobj("Tag", "sopdatagui"));
            sopSetGuiEnable(findobj("tag", "sopgui"), "on");
        end
        return // Avoid the execution of updateSopDataGui();
    case "responses_listbox"
        // Nothing to do
    case "responsestovariables_btn"
        sopProj = sopGetCurrentProject(h);
        responsesListbox = findobj("Tag", "responses_listbox");
        selectedResponses = responsesListbox.value;
        sopProj.variablesfromresponses = selectedResponses;
        sopSetCurrentProject(h, sopProj);
    case "reset_btn"
        // Reload data from original file
        sopProj = sopGetCurrentProject(h);
        mdelete(sopProj.workfile)
        sopProj = sopLoadDataFile(h, sopProj.pointedfile);
        sopSetCurrentProject(h, sopProj);
    case "save_btn"
        // Erase original file by modified one (workfile)
        sopProj = sopGetCurrentProject(h);
        sopSaveProject(sopProj);
    case "saveclose_btn"
        cbSopDataGui("save_btn");
        cbSopDataGui("close_btn");
        return
    case "selectsimulator_btn"
        allSimulators = sopLoadWrappers("simulator");
        allSimulatorsDesc = [];
        for kSimulator = 1:size(allSimulators, "*")
            [ierr, allSimulatorsDesc($+1)] = sopCallWrapper(allSimulators(kSimulator), "description");
            if ierr<>0 then
                error(msprintf(_("%s: Could not get description for ''%s'' wrapper.\n"), "cbSopDataGui", allSimulators(kSimulator)));
            end
        end
        selectedItem = x_choose(allSimulatorsDesc, _("Select a simulator"), _("Cancel"));
        if selectedItem==0 then // The user cancels
            return
        end

        sopProj = sopGetCurrentProject(h);

        sopProj.simulator = sopInitWrapper(allSimulators(selectedItem));

    case "simulateresponses_menu"
        sopProj = sopGetCurrentProject(h);
        sopSimulation(sopProj);
    case "variables_listbox"
        // Nothing to do
    case "variablestofactors_btn"
        sopProj = sopGetCurrentProject(h);
        variablesListbox = findobj("Tag", "variables_listbox");
        selectedVariables = variablesListbox.value;
        sopProj.factorsfromvariables = selectedVariables;
        sopSetCurrentProject(h, sopProj);
    case "variablestoresponses_btn"
        sopProj = sopGetCurrentProject(h);
        variablesListbox = findobj("Tag", "variables_listbox");
        selectedVariables = variablesListbox.value;
        sopProj.responsesfromvariables = selectedVariables;
        sopSetCurrentProject(h, sopProj);
    case "viewfactors_menu"
        sopGraphFactors();
        return // Avoid the execution of updateSopDataGui();
    case "viewresponses_menu"
        sopGraphResponses();
        return // Avoid the execution of updateSopDataGui();
    else
        error("Unknown object with tag: " + gcboTag);
    end

    updateSopDataGui();

endfunction
