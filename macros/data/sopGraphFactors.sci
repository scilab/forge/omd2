// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopGraphFactors()
    
    datagui = findobj("Tag", "sopdatagui");

    sopProj = sopGetCurrentProject(datagui);

    if size(sopProj.factors)==0 then
        messagebox("Project has no factors.", "Error", "error");
        return
    end

    // Visualization
    fig = sopFigure(_("Factors"));
    drawlater
    firstFact = 1;
    graphIndex = 0;
    for kFactY = 1:(size(sopProj.factors)-1)
        firstFact = firstFact + 1;
        for kFactX = firstFact:size(sopProj.factors)
            graphIndex = graphIndex + 1;
            a = sopSubplot(size(sopProj.factors)-1, size(sopProj.factors)-1, graphIndex, 0.02);
            plot(sopProj.factors(kFactX).values, sopProj.factors(kFactY).values, ".r", "MarkerSize", 2);
            sopSetAxesLabels(a, sopProj.factors(kFactX), sopProj.factors(kFactY))
            a.tight_limits = "on";
        end
        graphIndex = graphIndex + firstFact - 1;
    end
    drawnow
endfunction

