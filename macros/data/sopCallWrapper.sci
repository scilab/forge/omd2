// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [ierr, value] = sopCallWrapper(wrapperFunctionName, actionName, object)

    // Check input arguments number
    rhs = argn(2);
    if rhs<2 then
        error(msprintf(_("%s: Wrong number of input arguments: %d or %d expected.\n"), "sopCallWrapper", 2, 3));
    end
    
    if rhs==2  then
        object = [];
    end

    // Check input arguments number
    if typeof(wrapperFunctionName)<>"string" then
        error(msprintf(_("%s: Wrong type for input argument #%d: A string expected.\n"), "sopCallWrapper", 1));
    end
    if size(wrapperFunctionName, "*")<>1 then
        error(msprintf(_("%s: Wrong size for input argument #%d: A string expected.\n"), "sopCallWrapper", 1));
    end

    // First the function must exist
    if ~isdef(wrapperFunctionName) then
        error(msprintf(_("%s: Wrong value for input argument #%d: A defined function name expected.\n"), "sopCallWrapper", 1));
    end

    ierr = execstr("value = " + wrapperFunctionName + "(""" + actionName + """, object)", "errcatch");
endfunction

