// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function wrapperObj = sopInitWrapper(wrapperFunctionName)

    // Check input arguments number
    rhs = argn(2);
    if rhs<>1  then
        error(msprintf(_("%s: Wrong number of input argument: %d expected.\n"), "sopInitWrapper", 1));
    end

    // Check input arguments number
    if typeof(wrapperFunctionName)<>"string" then
        error(msprintf(_("%s: Wrong type for input argument #%d: A string expected.\n"), "sopInitWrapper", 1));
    end
    if size(wrapperFunctionName, "*")<>1 then
        error(msprintf(_("%s: Wrong size for input argument #%d: A string expected.\n"), "sopInitWrapper", 1));
    end

    // First the function must exist
    if ~isdef(wrapperFunctionName) then
        error(msprintf(_("%s: Wrong value for input argument #%d: A defined function name expected.\n"), "sopInitWrapper", 1));
    end

    // Try to get the wrapper type
    wrapperType = "unknown";
    [ierr, wrapperType] = sopCallWrapper(wrapperFunctionName, "type");
    if ierr<>0 then
        error(msprintf(_("%s: Could not get type for ''%s'' wrapper.\n"), "sopInitWrapper", wrapperFunctionName));
    end

    select wrapperType
    case "doegenerator"
        // Create the tlist
        wrapperObj = tlist(["sopdoege", "<version>", "<funname>", "<params>", "<values>"])

        // Set default values
        wrapperObj.version = evstr(wrapperFunctionName + "(""version"")");
        wrapperObj.funname = wrapperFunctionName;
        wrapperObj.params = evstr(wrapperFunctionName + "(""defaults"")");
        wrapperObj.values = [];

    case "simulator"
        // Create the tlist
        wrapperObj = tlist(["sopsimul", "<version>", "<funname>", "<params>", "<points>", "<values>"])

        // Set default values
        wrapperObj.version = evstr(wrapperFunctionName + "(""version"")");
        wrapperObj.funname = wrapperFunctionName;
        wrapperObj.params = evstr(wrapperFunctionName + "(""defaults"")");
        wrapperObj.points = [];
        wrapperObj.values = [];

    case "modeler"
        // Create the tlist
        wrapperObj = tlist(["sopmdl", "<version>", "<funname>", "<params>", "<coeffs>", "<points>", "<predictions>"])

        // Set default values
        wrapperObj.version = evstr(wrapperFunctionName + "(""version"")");
        wrapperObj.funname = wrapperFunctionName;
        wrapperObj.params = evstr(wrapperFunctionName + "(""defaults"")");
        wrapperObj.coeffs = [];
        wrapperObj.points = [];
        wrapperObj.predictions = [];

    case "optimizer"
        wrapperObj = tlist(["sopoptim", "<version>", "<funname>", "<params>", "<optimalpoint>", "<objfunctionoptimalvalue>"])

        // Set default values
        wrapperObj.version = evstr(wrapperFunctionName + "(""version"")");;
        wrapperObj.funname = wrapperFunctionName;
        wrapperObj.params = evstr(wrapperFunctionName + "(""defaults"")");
        wrapperObj.optimalpoint = [];
        wrapperObj.objfunctionoptimalvalue = [];

    else
        error(msprintf(_("%s: Unknown wrapper type ''%s''.\n"), "sopInitWrapper", wrapperType));
    end

endfunction

