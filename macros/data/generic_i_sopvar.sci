// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function variableObj = generic_i_sopvar(propertyName, propertyValue, variableObj)

    if typeof(variableObj)<>"sopvar" then
        error(msprintf("%s: Wrong type for input argument #%d: A ''%s'' tlist expected.\n", "generic_i_sopvar", 3, "sopvar"));
    end
    if typeof(propertyName)<>"string" then
        error(msprintf("%s: Wrong type for input argument #%d: A string expected.\n", "generic_i_sopvar", 1));
    end
    if size(propertyName, "*")<>1 then
        error(msprintf("%s: Wrong size for input argument #%d: A string expected.\n", "generic_i_sopvar", 1));
    end

    propertyName = "<" + convstr(propertyName, "l") + ">";

    select propertyName
    case "<indexinfile>"
        if typeof(propertyValue)<>"constant" then
            error(msprintf("Wrong type for ''%s'' property: A real expected.\n", "IndexInFile"));
        end
        if size(propertyValue, "*")<>1 then
            error(msprintf("Wrong size for ''%s'' property: A real expected.\n", "IndexInFile"));
        end
        if propertyValue<0 then
            error(msprintf("Wrong value for ''%s'' property: Must be greater or equal to %d.\n", "IndexInFile", 0));
        end
        variableObj(propertyName) = propertyValue;
    case "<isfixed>"
        if typeof(propertyValue)<>"boolean" then
            error(msprintf("Wrong type for ''%s'' property: A boolean expected.\n", "IsFixed"));
        end
        if size(propertyValue, "*")<>1 then
            error(msprintf("Wrong size for ''%s'' property: A boolean expected.\n", "IsFixed"));
        end
        variableObj(propertyName) = propertyValue;
    case "<maxbound>"
        if typeof(propertyValue)<>"constant" then
            error(msprintf("Wrong type for ''%s'' property: A real expected.\n", "MaxBound"));
        end
        if size(propertyValue, "*")<>1 then
            error(msprintf("Wrong size for ''%s'' property: A real expected.\n", "MaxBound"));
        end
        if ~isnan(variableObj.minbound) & propertyValue <= variableObj.minbound then
            error(msprintf("Wrong value for ''%s'' property: Must be greater than %d.\n", "MaxBound", variableObj.minbound));
        end        
        if ~isnan(variableObj.nominalvalue) & propertyValue < variableObj.nominalvalue then
            sopLog(msprintf("Nominal value greater than ''MaxBound'' for variable %s: nominal value reset to %%nan.\n", variableObj.name));
            variableObj.nominalvalue = %nan;
        end
        variableObj(propertyName) = propertyValue;
    case "<minbound>"
        if typeof(propertyValue)<>"constant" then
            error(msprintf("Wrong type for ''%s'' property: A real expected.\n", "MinBound"));
        end
        if size(propertyValue, "*")<>1 then
            error(msprintf("Wrong size for ''%s'' property: A real expected.\n", "MinBound"));
        end
        if ~isnan(variableObj.maxbound) & propertyValue >= variableObj.maxbound then
            error(msprintf("Wrong value for ''%s'' property: Must be lesser than %d.\n", "MinBound", variableObj.maxbound));
        end        
        if ~isnan(variableObj.nominalvalue) & propertyValue > variableObj.nominalvalue then
            sopLog(msprintf("Nominal value lesser than ''MinBound'' for variable %s: nominal value reset to %%nan.\n", variableObj.name));
            variableObj.nominalvalue = %nan;
        end
        variableObj(propertyName) = propertyValue;
    case "<models>"
        if typeof(propertyValue)<>"list" then
            error(msprintf("Wrong type for ''%s'' property: A list expected.\n", "Models"));
        end
        for kModel = 1:size(propertyValue)
            if typeof(propertyValue(kModel))<>"sopmdl" then
                error(msprintf("%s: Wrong type for ''%s'' property (item #%d): A ''%s'' tlist expected.\n", "set", "Models", kModel, "sopmdl"));
            end
        end
        variableObj(propertyName) = propertyValue;
    case "<name>"
        if typeof(propertyValue)<>"string" then
            error(msprintf("Wrong type for ''%s'' property: A string expected.\n", "Name"));
        end
        if size(propertyValue, "*")<>1 then
            error(msprintf("Wrong size for ''%s'' property: A string expected.\n", "Name"));
        end
        variableObj(propertyName) = propertyValue;
    case "<nominalvalue>"
        if typeof(propertyValue)<>"constant" then
            error(msprintf("Wrong type for ''%s'' property: A real expected.\n", "NominalValue"));
        end
        if size(propertyValue, "*")<>1 then
            error(msprintf("Wrong size for ''%s'' property: A real expected.\n", "NominalValue"));
        end
        if ~isnan(variableObj.minbound) & propertyValue < variableObj.minbound then
            error(msprintf("Wrong value for ''%s'' property: Must be greater than %d.\n", "NominalValue", variableObj.minbound));
        end        
        if ~isnan(variableObj.maxbound) & propertyValue > variableObj.maxbound then
            error(msprintf("Wrong value for ''%s'' property: Must be lesser than %d.\n", "NominalValue", variableObj.maxbound));
        end        
        variableObj(propertyName) = propertyValue;
    case "<units>"
        if typeof(propertyValue)<>"string" then
            error(msprintf("Wrong type for ''%s'' property: A string expected.\n", "Units"));
        end
        if size(propertyValue, "*")<>1 then
            error(msprintf("Wrong size for ''%s'' property: A string expected.\n", "Units"));
        end
        variableObj(propertyName) = propertyValue;
    case "<values>"
        // Must be a vector
        if ~isempty(propertyValue) & size(propertyValue, 2)<>1 then
            error(msprintf("Wrong size for ''%s'' property: A column vector expected.\n", "Values"));
        end
        variableObj(propertyName) = propertyValue;
    case "<valuestype>"
        // Must be a vector of the same size as 'values'
        if typeof(propertyValue)<>"constant" then
            error(msprintf("Wrong type for ''%s'' property: A real column vector expected.\n", "ValuesType"));
        end
        if ~isempty(propertyValue) & size(propertyValue, 2)<>1 then
            error(msprintf("Wrong size for ''%s'' property: A real column vector expected.\n", "ValuesType"));
        end
        if or(size(propertyValue) <> size(variableObj.values)) then
            error(msprintf("Wrong size for ''%s'' property: A real %dx%d vector expected.\n", "ValuesType", size(variableObj.values, 1), 1));
        end
        // valuestype = 0 ==> Bad Point
        // valuestype = 1 ==> Good point
        // valuestype = 2 ==> Validation point
        variableObj(propertyName) = propertyValue;
    else
        error(msprintf("''%s'' is not a valid property name.\n", propertyName));
    end

endfunction

