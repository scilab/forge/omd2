// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopVariableGui(isFactor, callingGcboTag, editedVariable#)
    
    sopdatagui = findobj("Tag", "sopdatagui");
    sopSetGuiEnable(sopdatagui, "off");
    
    [figbg, margin, listboxbg, btnbg, btnh] = sopGuiParameters("FigureBackground", ..
                                                               "Margin", ..
                                                               "ListboxBackground", ..
                                                               "ButtonBackground", ..
                                                               "ButtonHeight");
                                                               

    if argn(2) == 2 then
        editedVariable = sopVariable();
    end
    
    if isFactor then
        guiTitle = _("Factor edition");
    else
        guiTitle = _("Response edition");
    end
    
    parameters = ["Fixed" "Max value", "Nominal value" "Min value" "Name"];
    fields = ["isfixed" "maxbound" "nominalvalue" "minbound" "name"];
    nbParams = size(parameters, "*")

    figw = 200 + 5*margin;
    figh = 4*margin + nbParams*(btnh+margin) + btnh;
    figx = sopdatagui.position(1) + sopdatagui.position(3)/2 - figw/2;
    figy = sopdatagui.position(2) + sopdatagui.position(4)/2 - figh/2;

    h = sopFigure(guiTitle, [figx figy figw figh], "sopvariablegui", [], %T, %T)
    ud = struct();
    ud.sopeditedvariable = editedVariable#;
    h.userdata = ud;
    
    // Add validation button
    btnw = 60;
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "Position", [figw-margin-btnw margin btnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "String", _("Ok"), ..
              "Callback", "cbSopVariableGui();", ..
              "Tag", "ok_btn");
    uicontrol("Parent", h, ..
              "Style", "pushbutton", ..
              "Position", [figw-2*margin-2*btnw margin btnw btnh], ..
              "BackgroundColor", btnbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "String", _("Cancel"), ..
              "Callback", "cbSopVariableGui();", ..
              "Tag", "cancel_btn");
              
    // Configuration frame
    framex = margin;
    framey = 2*margin + btnh;
    framew = 200 + 3*margin;
    frameh = nbParams*(btnh+margin) + margin;
    sopFrameWithTitle(h, [framex, framey, framew, frameh], _("Settings"), 70, "settings");
    
    // "Fixed" checkbox
    enable = "off";
    if isFactor then
        enable = "on";
    end
    kParam = 1;
    uicontrol("Parent", h, ..
              "Style", "text", ..
              "Position", [framex+margin framey+margin+(kParam-1)*(btnh+margin) 100 btnh], ..
              "BackgroundColor", figbg, ..
              "FontWeight", "bold", ..
              "String", parameters(kParam), ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", enable, ..
              "Tag", fields(kParam) + "_label");
    // Checkbox
    uicontrol("Parent", h, ..
              "Style", "checkbox", ..
              "Position", [framex+2*margin+100 framey+margin+(kParam-1)*(btnh+margin) 100 btnh], ..
              "BackgroundColor", figbg, ..
              "FontName", "Arial", ..
              "FontSize", 12, ..
              "Enable", enable, ..
              "Min", 0, ..
              "Max", 1, ..
              "Value", bool2s(editedVariable#(fields(kParam))), ..
              "Callback", "cbSopVariableGui();", ..
              "Tag", fields(kParam));

    // For each parameter (except "Fixed"), add an entry
    for kParam = nbParams:-1:2
        uicontrol("Parent", h, ..
                  "Style", "text", ..
                  "Position", [framex+margin framey+margin+(kParam-1)*(btnh+margin) 100 btnh], ..
                  "BackgroundColor", figbg, ..
                  "FontWeight", "bold", ..
                  "String", parameters(kParam), ..
                  "FontName", "Arial", ..
                  "FontSize", 12, ..
                  "Enable", "on", ..
                  "Tag", fields(kParam) + "_label");
        // Edit
        enabled = "on";
        if ~isFactor & kParam<>nbParams then
            enabled = "off";
        end
        uicontrol("Parent", h, ..
                  "Style", "edit", ..
                  "Position", [framex+2*margin+100 framey+margin+(kParam-1)*(btnh+margin) 100 btnh], ..
                  "BackgroundColor", [1 1 1], ..
                  "String", string(editedVariable#(fields(kParam))), ..
                  "FontName", "Arial", ..
                  "FontSize", 12, ..
                  "Enable", enabled, ..
                  "Callback", "cbSopVariableGui();", ..
                  "Tag", fields(kParam));
    end
    
    isFixed = get(findobj("Tag", "isfixed"), "Value") == get(findobj("Tag", "isfixed"), "Max");
    if isFixed then
        set(findobj("Tag", "minbound"), "Enable", "off");
        set(findobj("Tag", "maxbound"), "Enable", "off");
    else
        set(findobj("Tag", "minbound"), "Enable", "on");
        set(findobj("Tag", "maxbound"), "Enable", "on");
    end


endfunction
