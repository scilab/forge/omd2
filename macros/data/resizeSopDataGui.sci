// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2013 - Scilab Enterprises - Vincent COUVERT
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function resizeSopDataGui()

    fig = findobj("Tag", "sopdatagui");
    hdls = sopGuiHandles(fig);

    [margin, btnh] = sopGuiParameters("Margin", ..
                                      "ButtonHeight");

    figh = fig.position(4);
    framemaxh = figh - btnh - 3*margin;
    figw = fig.position(3);

    btnw = 90;
    hdls.close_btn.position = [figw-margin-btnw margin btnw btnh];
    hdls.saveclose_btn.position = [figw-2*margin-2*btnw margin btnw btnh];
    hdls.save_btn.position = [figw-3*margin-3*btnw margin btnw btnh];
    hdls.reset_btn.position = [figw-4*margin-4*btnw margin btnw btnh];
    
    frameybasis = 2*margin+btnh;
    // Variables frame
    framex = margin;
    framey = frameybasis;
    framew = (figw-(5*margin + 40))/3;
    frameh = framemaxh;
    hdls.variables_frame.position = [framex, framey, framew, frameh];
    hdls.variables_title.position(1:2) = [framex+margin framey+frameh-7];
    hdls.variables_listbox.position = [framex+margin framey+margin framew-2*margin frameh-2*margin];
    
    miniframeh = (figh-3*margin)/2;
    btnw = 40;
    hdls.variablestoresponses_btn.position = [2*margin+framew margin+miniframeh/2+margin btnw btnh];
    hdls.responsestovariables_btn.position = [2*margin+framew margin+miniframeh/2-margin-btnh btnw btnh]
    hdls.variablestofactors_btn.position = [2*margin+framew margin+miniframeh*3/2+2*margin btnw btnh];
    hdls.factorstovariables_btn.position = [2*margin+framew margin+miniframeh*3/2-btnh btnw btnh];

    // Responses frame
    framex = 3*margin + framew + btnw;
    framey = frameybasis;
    framew = (figw-(5*margin + 40))/3;
    frameh = (framemaxh-margin)/2;
    hdls.responses_frame.position = [framex, framey, framew, frameh];
    hdls.responses_title.position(1:2) = [framex+margin framey+frameh-7];
    hdls.responses_listbox.position = [framex+margin framey+2*margin+btnh framew-2*margin frameh-3*margin-btnh];
    tmpbtnw = (framew-4*margin)/3;
    hdls.addresponse_btn.position = [framex+margin framey+margin tmpbtnw btnh];
    hdls.delresponse_btn.position = [framex+2*margin+tmpbtnw framey+margin tmpbtnw btnh];
    hdls.editresponse_btn.position = [framex+3*margin+2*tmpbtnw framey+margin tmpbtnw btnh];

    // Factors frame
    framex = 3*margin + framew + btnw;
    framey = frameh + 3*margin + btnh;
    framew = (figw-(5*margin + 40))/3;
    frameh = (framemaxh-margin)/2;
    hdls.factors_frame.position = [framex, framey, framew, frameh];
    hdls.factors_title.position(1:2) = [framex+margin framey+frameh-7];
    hdls.factors_listbox.position = [framex+margin framey+2*margin+btnh framew-2*margin frameh-3*margin-btnh];
    tmpbtnw = (framew-4*margin)/3;
    hdls.addfactor_btn.position = [framex+margin framey+margin tmpbtnw btnh];
    hdls.delfactor_btn.position = [framex+2*margin+tmpbtnw framey+margin tmpbtnw btnh];
    hdls.editfactor_btn.position = [framex+3*margin+2*tmpbtnw framey+margin tmpbtnw btnh];

    // Responses configuration frame
    framex = 4*margin + 2*framew + btnw;
    framey = frameybasis;
    framew = (figw-(5*margin + 40))/3;
    frameh = (framemaxh-margin)/2;
    tmpbtnw = (framew-4*margin)/3
    hdls.respconfig_frame.position = [framex, framey, framew, frameh];
    hdls.respconfig_title.position(1:2) = [framex+margin framey+frameh-7];
    hdls.configuresimulator_btn.position = [framex+margin framey+margin framew-2*margin btnh];
    hdls.selectsimulator_btn.position = [framex+margin framey+2*margin+btnh framew-2*margin btnh];
    hdls.simulatorname_txt.position = [framex+margin framey+3*margin+2*btnh framew-2*margin btnh];

    // Factors configuration frame
    framex = 4*margin + 2*framew + btnw;
    framey = frameh + 3*margin + btnh;
    framew = (figw-(5*margin + 40))/3;
    frameh = (framemaxh-margin)/2;
    tmpbtnw = (framew-4*margin)/3
    hdls.factconfig_frame.position = [framex, framey, framew, frameh];
    hdls.factconfig_title.position(1:2) = [framex+margin framey+frameh-7];
    hdls.generators_listbox.position = [framex+margin framey+2*margin+btnh framew-2*margin frameh-3*margin-btnh];
    tmpbtnw = (framew-4*margin)/3;
    hdls.addgenerator_btn.position = [framex+margin framey+margin tmpbtnw btnh];
    hdls.delgenerator_btn.position = [framex+2*margin+tmpbtnw framey+margin tmpbtnw btnh];
    hdls.editgenerator_btn.position = [framex+3*margin+2*tmpbtnw framey+margin tmpbtnw btnh];
 
endfunction
