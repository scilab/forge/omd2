// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopProj = sopLoadDataFile(sopProj, dataFilePath, forceValidationType)

    sopproj = [];

    if argn(2)<3 then
        forceValidationType = %F;
    end

    if ~isfile(dataFilePath) then
        error("File does not exists: " + dataFilePath);  // TODO add better error message
    end

    extension = fileparts(dataFilePath, "extension");

    sopLog("Loading file (" + extension + "): " + dataFilePath);
    select extension
    case ".sop"
        sopProj = sopLoadProject(dataFilePath);
    case ".db"
        sopProj = importDataFromTextFile(sopProj, dataFilePath, "\t", forceValidationType)
    case ".txt"
        sopProj = importDataFromTextFile(sopProj, dataFilePath, "\t", forceValidationType);
    else
        sopError("Unknown file format: " + extension);
        return
    end

    sopProj.dataexecstatus = sopStatus("Done");

    sopLog("File successfully loaded.");

endfunction

function curProj = importDataFromTextFile(curProj, dataFilePath, separator, forceValidationType)

    datafile = read_csv(dataFilePath, separator);

    // Create the list of variables
    allVariables = list();
    for kRow = 1:size(datafile, "c")
        variable = sopVariable();
        variableName = stripblanks(datafile(1, kRow));
        if isempty(variableName) then // Empty name then ignored
            sopLog("Variable with empty name ignored.");
            continue
        end
        variable.name = variableName;
        variable.indexinfile = kRow;
        [variableValues, ierr] = evstr(datafile(2:$, kRow));
        if ierr<>0 then
            variableValues = datafile(2:$, kRow);
        end
        variable.values = variableValues
        variable.valuestype = ones(size(variableValues,1),1)
        allVariables($+1) = variable;
        sopLog("Variable " + string(kRow) + ": " + variableName + " loaded.");
    end

    // Ensure that all variables have the same number of values
    maxSize = 0;
    for kVar = 1:size(allVariables)
        maxSize = max(size(allVariables(kVar).values, "*"), maxSize);
    end
    for kVar = 1:size(allVariables)
        if size(allVariables(kVar).values, "*") < maxSize then
            valuesType = allVariables(kVar).valuestype;
            allVariables(kVar).values = [allVariables(kVar).values; %nan*zeros(maxSize - size(allVariables(kVar).values, "*"), 1)]
            allVariables(kVar).valuestype = [valuesType; zeros(maxSize - size(valuesType, "*"), 1)]
        end
    end

    if forceValidationType then
        filePointType = 2;
    else
        filePointType = 1;
    end

    fileVarNames = [];
    for kVar = 1:size(allVariables)
        fileVarNames($+1) = allVariables(kVar).name;
    end
    fileEntriesNumber = size(allVariables(kVar).values, 1);

    projVarNames = [];
    for kVar = 1:size(curProj.variables)
        projVarNames($+1) = curProj.variables(kVar).name;
    end
    projEntriesNumber = 0; // Empty project
    if size(curProj.variables)<>0 then
        projEntriesNumber = size(curProj.variables(1).values, 1);
    end
    projFactNames = [];
    for kFact = 1:size(curProj.factors)
        projFactNames($+1) = curProj.factors(kFact).name;
    end
    if size(curProj.factors)<>0 then
        projEntriesNumber = size(curProj.factors(1).values, 1);
    end
    projRespNames = [];
    for kResp = 1:size(curProj.responses)
        projRespNames($+1) = curProj.responses(kResp).name;
    end
    if size(curProj.responses)<>0 then
        projEntriesNumber = size(curProj.responses(1).values, 1);
    end

    // Add fake values to variables read in the file (at the beginning) if they does not already exist
    newVariablesIndexes = [];
    for kVar = 1:size(allVariables)
        if ~or(allVariables(kVar).name==projVarNames) & ~or(allVariables(kVar).name==projFactNames) & ~or(allVariables(kVar).name==projRespNames) then
            // Unknown variable
            valuesType = filePointType*ones(fileEntriesNumber, 1);
            allVariables(kVar).values = [%nan*zeros(projEntriesNumber, 1) ; allVariables(kVar).values]
            allVariables(kVar).valuestype = [zeros(projEntriesNumber, 1) ; valuesType]
            newVariablesIndexes($+1) = kVar;
        end
    end

    // Add fake values to existing variables/factors/responses if not found in the new file (at the end)
    allProjVariables = curProj.variables;
    for kVar = 1:size(allProjVariables)
        if ~or(allProjVariables(kVar).name==fileVarNames) then
            // Unknown variable in file ==> Create fake points
            valuesType = allProjVariables(kVar).valuestype;
            allProjVariables(kVar).values = [allProjVariables(kVar).values ; %nan*zeros(fileEntriesNumber, 1)];
            allProjVariables(kVar).valuestype = [valuesType ; zeros(fileEntriesNumber, 1)];
        else
            // Existing variable in file ==> Catenate data
            varIndex = find(allProjVariables(kVar).name==fileVarNames);
            valuesType = allProjVariables(kVar).valuestype;
            allProjVariables(kVar).values = [allProjVariables(kVar).values ; allVariables(varIndex).values];
            allProjVariables(kVar).valuestype = [valuesType ; filePointType*ones(fileEntriesNumber, 1)];
        end
    end
    curProj.variables = allProjVariables;

    // Add fake values to existing factors if not found in the new file (at the end)
    allProjFactors = curProj.factors;
    for kFact = 1:size(allProjFactors)
        if ~or(allProjFactors(kFact).name==fileVarNames) then
            // Unknown factor in file ==> Create fake points
            valuesType = allProjFactors(kFact).valuestype;
            allProjFactors(kFact).values = [allProjFactors(kFact).values ; %nan*zeros(fileEntriesNumber, 1)];
            allProjFactors(kFact).valuestype = [valuesType ; zeros(fileEntriesNumber, 1)];
        else
            // Existing factor in file ==> Catenate data
            varIndex = find(allProjFactors(kFact).name==fileVarNames);
            valuesType = allProjFactors(kFact).valuestype;
            allProjFactors(kFact).values = [allProjFactors(kFact).values ; allVariables(varIndex).values];
            allProjFactors(kFact).valuestype = [valuesType ; filePointType*ones(fileEntriesNumber, 1)];
        end
    end
    curProj.factors = allProjFactors;

    // Add fake values to existing responses if not found in the new file (at the end)
    allProjResponses = curProj.responses;
    for kResp = 1:size(allProjResponses)
        if ~or(allProjResponses(kResp).name==fileVarNames) then
            // Unknown response in file ==> Create fake points
            valuesType = allProjResponses(kResp).valuestype;
            allProjResponses(kResp).values = [allProjResponses(kResp).values ; %nan*zeros(fileEntriesNumber, 1)];
            allProjResponses(kResp).valuestype = [valuesType ; zeros(fileEntriesNumber, 1)];
        else
            // Existing response in file ==> Catenate data
            varIndex = find(allProjResponses(kResp).name==fileVarNames);
            valuesType = allProjResponses(kResp).valuestype;
            allProjResponses(kResp).values = [allProjResponses(kResp).values ; allVariables(varIndex).values];
            allProjResponses(kResp).valuestype = [valuesType ; filePointType*ones(fileEntriesNumber, 1)];
        end
    end
    curProj.responses = allProjResponses;

    // Add new variables (did not exist in current project as variable, factor or response)
    allProjVariables = curProj.variables;
    for kVar = newVariablesIndexes'
        allProjVariables($+1) = allVariables(kVar)
    end
    curProj.variables = allProjVariables;

endfunction

