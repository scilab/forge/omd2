// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sopSimulation(sopProj)

    rhs = argn(2);
    if rhs<>1 then
        error(msprintf(gettext("%s: Wrong number of input arguments: %d expected.\n"), "sopSimulation", 1))
    end
    if sopObjectType(sopProj)<>"Project" then
        error(msprintf(gettext("%s: Wrong type for input argument #%d: A SOP object of type ''%s'' expected.\n"), "sopSimulation", 1, "Project"));
    end

    sopLog("Simulation...");
    sopProj.dataexecstatus = sopStatus("Running");

    sopLog(_("Reset all responses values."));
    allResponses = sopProj.responses#;
    for kResp = 1:size(allResponses)
        allResponses(kResp).values = [];
        allResponses(kResp).valuestype = [];
    end

    // Call the simulator
    simulatorObj = sopProj.simulator#;
    simulatorFunction = simulatorObj.funname;
    // Initialize the points to simulate
    pointsToSimulate = [];
    allFactors = sopProj.factors
    for kFact = 1:size(allFactors)
        pointsToSimulate = [pointsToSimulate allFactors(kFact).values]
    end
    simulatorObj.points = pointsToSimulate;
    ierr = sopCallWrapper(simulatorFunction, "simulate", simulatorObj);
    if ierr<>0 then
        sopProj.dataexecstatus = sopStatus("Failed");
        sopError("Simulation: FAILED");
        return
    end

    // Update project with simulator output arguments values
    sopLog(_("Update responses values."));
    values = simulatorObj.values;
    for kResp = 1:size(allResponses)
        allResponses(kResp).values = values(:, kResp);
        valuesType = ones(size(values(:, kResp), 1), 1); // Learning point
        valuesType(isnan(values(:, kResp))) = 0; // Failed simulations ==> Bad point
        allResponses(kResp).valuestype = valuesType;
    end

    sopProj.dataexecstatus = sopStatus("Done");
    sopLog("Simulation: DONE");
    
endfunction