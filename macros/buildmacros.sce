// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012 - DIGITEO - Vincent COUVERT
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

sopPath = get_absolute_file_path("buildmacros.sce");

if ~isdef("genlib") then
  exec(SCI+"/modules/functions/scripts/buildmacros/loadgenlib.sce");
end

genlib("sopthirdpartylib"   , sopPath + "thirdparty/"  , %f,%f);
genlib("sopwrapperlib"      , sopPath + "wrapper/"     , %f,%f);
genlib("sopdatalib"         , sopPath + "data/"        , %f,%f);
genlib("sopmodelinglib"     , sopPath + "modeling/"    , %f, %f);
genlib("sopoptimizationlib" , sopPath + "optimization/", %f, %f);
genlib("sopcorelib"         , sopPath + "core/"        , %f, %f);
genlib("sopsomtoolboxlib"   , sopPath + "somtoolbox/"  , %f, %f);
genlib("sopmvclib"          , sopPath + "mvc/"  , %f, %f);
// Internal library: not loaded at startup
genlib("sopmvc_internalslib", sopPath + "mvc/mvc_internals/"  , %f, %f);

clear sopPath;
