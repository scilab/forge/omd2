// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function propertyValue = generic_e_sopoptim(propertyName, optimizerObj)

    if typeof(optimizerObj)<>"sopoptim" then
        error(msprintf("%s: Wrong type for input argument #%d: A ''%s'' tlist expected.\n", "generic_e_sopoptim", 3, "sopoptim"));
    end
    if typeof(propertyName)<>"string" then
        error(msprintf("%s: Wrong type for input argument #%d: A string expected.\n", "generic_e_sopoptim", 1));
    end
    if size(propertyName, "*")<>1 then
        error(msprintf("%s: Wrong size for input argument #%d: A string expected.\n", "generic_e_sopoptim", 1));
    end
    
    propertyName = "<" + convstr(propertyName, "l") + ">";

    select propertyName
    case "<funname>"
        propertyValue = optimizerObj(propertyName);
    case "<objfunctionoptimalvalue>"
        propertyValue = optimizerObj(propertyName);
    case "<optimalpoint>"
        propertyValue = optimizerObj(propertyName);
    case "<params>"
        propertyValue = optimizerObj(propertyName);
    case "<version>"
        propertyValue = optimizerObj(propertyName);
    else
        error(msprintf("''%s'' is not a valid property name.\n", propertyName));
    end

endfunction


