// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [objectiveFunctionValue, objectiveFunctionGrad, ind] = sopObjectiveFunction(factorsValues, ind, sopproj)

    // TODO check inputs

    nominalValues = [];
    for kFact = 1:size(sopproj.factors)
        nominalValues = [nominalValues, sopproj.factors(kFact).nominalvalue];
    end
    objectiveFunctionGrad = factorsValues-nominalValues;

    sopLog("Point to predict : " + sci2exp(factorsValues))

    objectiveFunctionValue = 0;
    if %F then // Optimize using simulation
        // Call the simulator
        simulatorObj = sopproj.simulator#;
        simulatorFunction = simulatorObj.funname;
        simulatorObj.points = factorsValues;
        ierr = sopCallWrapper(simulatorFunction, "simulate", simulatorObj);
        if ierr<>0 then
            sopproj.dataexecstatus = sopStatus("Failed");
            sopError("Simulation: FAILED");
            return
        end
        values = simulatorObj.values(1,:);
        for kResp = 1:size(sopproj.responses)
            objectiveFunctionValue = objectiveFunctionValue + sopproj.responsescoeffs(kResp) * values(kResp);
        end
    else // Optimize the model
        for kResp = 1:size(sopproj.responses)
            // TODO use best model instead of first model
            allResponses = sopproj.responses#;
            allModels = allResponses(kResp).models#
            allModels(1).points = factorsValues;
            sopLog("Coeff for response #" + string(kResp) + " = " + string(sopproj.responsescoeffs(kResp)))
            allResponses = sopproj.responses#;
            allModels = allResponses(kResp).models#;
            sopPrediction(allModels(1));
            sopLog("Prediction for response #" + string(kResp) + " = " + string(sopproj.responses(kResp).models(1).predictions))
            objectiveFunctionValue = objectiveFunctionValue + sopproj.responsescoeffs(kResp) * sopproj.responses(kResp).models(1).predictions;
        end
    end
endfunction
