// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function updateSopOptimizationGui()

    sopoptimgui = findobj("Tag", "sopoptimizationgui");
    
    sopproject = sopGetCurrentProject(sopoptimgui);
    
    if isempty(sopproject) then // At application start
        return
    end
    
    // Menus
    fileMenu = findobj("Tag", "file_menu");
    fileMenu.enable = "on";
    optimizationMenu = findobj("Tag", "optimization_menu");
    if size(sopproject.responses)==0 then
        optimizationMenu.enable = "off";
    else
        optimizationMenu.enable = "on";
    end

    // Window management buttons
    hdls = sopGuiHandles(sopoptimgui);
    hdls.reset_btn.enable = "on";
    hdls.save_btn.enable = "on";
    hdls.saveclose_btn.enable = "on";
    hdls.close_btn.enable = "on";

    // Responses frame
    responsesListbox = findobj("Tag", "responses_listbox");
    projectResponses = sopproject.responses;
    if size(projectResponses)<>0 then
        respNames = [];
        for kResp = 1:size(projectResponses)
            respNames($+1) = projectResponses(kResp).name;
        end
        if or(respNames<>responsesListbox.string') then
            responsesListbox.string = respNames;
        end
        responsesListbox.enable = "on";
    else
        responsesListbox.enable = "off";
        responsesListbox.string = "";
    end

    // Response coeff
    responsecoeffEdit = findobj("Tag", "responsecoeff_edit")
    responsecoeffBtn = findobj("Tag", "responsecoeff_btn")
    [newCoeff, ierr] = evstr(get(responsecoeffEdit, "String"));
    if ierr<>0 then
        set(findobj("Tag", "responsecoeff_edit"), "BackgroundColor", [1 0 0]);
        responsecoeffBtn.enable = "off";
    else
        set(findobj("Tag", "responsecoeff_edit"), "BackgroundColor", [1 1 1]);
        responsecoeffBtn.enable = "on";
    end

    selectedResponse = responsesListbox.value;
    if ~isempty(selectedResponse) & selectedResponse<>0 then
        responsecoeffEdit.enable = "on";
        responsecoeffEdit.string = string(sopproject.responsescoeffs(selectedResponse))
    else
        responsecoeffEdit.enable = "off";
        responsecoeffEdit.string = "";
    end

    // Optimizer #1
    optimizername1Txt = findobj("Tag", "optimizername1_txt");
    selectoptimizer1Btn = findobj("Tag", "selectoptimizer1_btn");
    configureoptimizer1Btn = findobj("Tag", "configureoptimizer1_btn");
    selectoptimizer1Btn.enable = "on";
    optimizername1Txt.enable = "on";
    if size(sopproject.optimizers)<>0 then
        configureoptimizer1Btn.enable = "on";
        optimizername1Txt.string = sopproject.optimizers(1).funname;
    else
        configureoptimizer1Btn.enable = "off";
        optimizername1Txt.string = _("No optimizer selected.");
    end
    
    // Optimizer #2
    optimizername2Txt = findobj("Tag", "optimizername2_txt");
    selectoptimizer2Btn = findobj("Tag", "selectoptimizer2_btn");
    configureoptimizer2Btn = findobj("Tag", "configureoptimizer2_btn");
    if configureoptimizer1Btn.enable == "on" then // First optimizer has been selected
        selectoptimizer2Btn.enable = "on";
    else
        selectoptimizer2Btn.enable = "off";
    end
    optimizername2Txt.enable = "on";
    if size(sopproject.optimizers) > 1 & configureoptimizer1Btn.enable == "on" then
        configureoptimizer2Btn.enable = "on";
        optimizername2Txt.string = sopproject.optimizers(2).funname;
    else
        configureoptimizer2Btn.enable = "off";
        optimizername2Txt.string = _("No optimizer selected.");
    end

endfunction

