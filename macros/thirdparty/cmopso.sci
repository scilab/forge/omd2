// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012 - DIGITEO - Vincent COUVERT 
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [ERP,vERP,idc,x]=cmopso(cout,niter,npop,nERP,xmin,xmax)
    //parametres de l'algo
    global neval
    neval=0;
    tic
    omega = 0.8;
    c1 = 1;
    c2 = 2;
    c2=0.5;
    omega=0.9;

    ndim = length(xmin);
    delta = (xmax - xmin)/5;
    x=rand(npop,ndim);
    v=zeros(npop,ndim);
    for i=1:ndim
        x(:,i) = (xmax(i)-xmin(i))*x(:,i)+xmin(i);
    end

    [f,con]=cout(x);

    nfunc=size(f,2);// f(npop,nfunc)
    ncon=size(con,2); // con(npop,ncon)

    idc = true(npop,1);
    for i=1:npop
        for j=1:ncon
            if(con(i,j) > 0)
                idc(i) = %F;
            end
        end
    end

    feas_hist=idc;

    ff=f(idc,:);
    xf=x(idc,:);
    front=paretofront(ff);

    ERP=xf(front,:);
    vERP=ff(front,:);

    nV=size(ERP,1);


    if(nV == 0)
        cv=zeros(npop,1);
        for i=1:npop
            for j=1:ncon
                cv(i) = cv(i) + max(0,con(i,j));
            end
        end
        [cvmin,imin]=min(cv);
        gbestc = x(imin,:);
        cv_hist = cv;
    else
        cv_hist = zeros(npop,1);
    end


    pbest = x;
    fpbest = f;
    ipbest = idc;

    gbest=zeros(size(x));

    nV = size(ERP,1);
    if(nV == 0)
        for i=1:npop
            gbest(i,:)=gbestc;
        end
    else
        n = randi(nV,[1,npop]);
        gbest(1:npop,:)=ERP(n,:);

        n1 = randi(floor(nV/2),[1,floor(npop/2)]);
        gbest(1:floor(npop/2),:)=ERP(n1,:);


    end

    for it=1:niter
        nV = size(ERP,1);

        if(nV == 0)
            for i=1:npop
                gbest(i,:)=gbestc;
            end
        else
            n = randi(nV,[1,npop]);
            gbest(1:npop,:)=ERP(n,:);
        end


        for i=1:ndim
            r1 = rand(1);
            r2 = rand(1);
            v(:,i) = omega*v(:,i) + c1*r1*(pbest(:,i)-x(:,i)) ...
            + c2*r2*(gbest(:,i) - x(:,i));
            v(:,i) = min(delta(i),v(:,i));
            v(:,i) = max(-delta(i),v(:,i));
            x(:,i) = x(:,i) + v(:,i);
            x(:,i) = max(xmin(i),x(:,i));
            x(:,i) = min(xmax(i),x(:,i));
        end

        [f,con] = cout(x);




        idc = true(npop,1);

        for i=1:npop
            for j=1:ncon
                if(con(i,j) > 0)
                    idc(i) = false;
                end
            end
        end



        for k=1:npop
            if(feas_hist(k))
                if(idc(k))
                    id = 1;
                    for i=1:nfunc
                        if(fpbest(k,i) > f(k,i))
                            id = 0;
                        end
                    end
                    if id == 0
                        pbest(k,:) = x(k,:);
                        fpbest(k,:) = f(k,:);
                    end
                end
            else
                if(idc(k))
                    feas_hist(k)=idc(k);
                    pbest(k,:)=x(k,:);
                    fpbest(k,:)=f(k,:);
                else
                    cvc = 0;
                    for i=1:ncon
                        cvc = cvc + max(0,con(k,i));
                    end
                    if(cvc <= cv_hist(k))
                        cv_hist(k) = cvc;
                        pbest(k,:)=x(k,:);
                    end
                end
            end
        end


        TempERP = ERP;
        VTERP   = vERP;
        [nT,bid]=size(TempERP);

        ERP=[];
        ff=f(idc,:);
        xf=x(idc,:);
        front=paretofront(ff);

        TempERP = [TempERP;xf(front,:)];
        VTERP = [VTERP;ff(front,:)];

        front = paretofront(VTERP);

        ERP = TempERP(front,:);
        vERP = VTERP(front,:);


        nV=size(ERP,1);



        if(nV > nERP)

            while nV > nERP        
                dist=inf(nV,1);
                for i=1:nV
                    for j=1:nV
                        if(i~=j)
                            dd = 0;  
                            //                  dd=sum(abs(vERP(i,:)-vERP(j,:)));
                            for k=1:nfunc
                                dd = dd + abs(vERP(i,k)-vERP(j,k));
                            end
                            dist(i) = min(dist(i),dd);
                        end
                    end
                end
                crowdist=[];
                crowdist=dist;
                for i=1:nfunc
                    [val,ix]=gsort(vERP(:,i),"g", "i");
                    crowdist(ix(1))=inf;
                    crowdist(ix(nV))=inf;
                end

                [crs,ic]=gsort(crowdist,"g", "d");



                nERPT=nV-1;

                //          plot(vERP(ic(1:nERPT),1),vERP(ic(1:nERPT),2),'r.',...
                //               vERP(ic(nERPT+1:nV),1),vERP(ic(nERPT+1:nV),2),'g.')
                //                    
                //           pause(.01)

                TempERP=[];
                VTERP=[];

                TempERP(1:nERPT,:)=ERP(ic(1:nERPT),:);
                VTERP(1:nERPT,:)=vERP(ic(1:nERPT),:);

                ERP=[];
                vERP=[];
                ERP=TempERP;
                vERP=VTERP;

                nV=nV-1;
            end

        end


        nV = size(ERP,1);
        if(nV == 0)
            cv=zeros(npop,1);
            for i=1:npop
                for j=1:ncon
                    cv(i) = cv(i) + max(0,con(i,j));
                end
            end
            [cmin,imin]=min(cv);
            if(cmin <= cvmin)
                cvmin = cmin;
                gbestc = x(imin,:);
            end

        end


    end

    toc
    disp(neval)
endfunction